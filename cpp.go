// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"

/* this file implements a C preprocessor */

type CppState struct {
	lex    *Lexer
	parent *CppState

	// stack for #if..#endif handling
	stack []BlkMode

	// the normal tokens (result of preprocessing)
	lines []*LexLine
}

type BlkMode int

const (
	BM_ACTIVE    BlkMode = 0  // block is currently active
	BM_INACTIVE  BlkMode = 1  // block not active (and none has been)
	BM_WASACTIVE BlkMode = 2  // block not active, but a previous one was
)

var all_defines map[string]*Define

type Define struct {
	name    string
	funky   bool      // false = object-like, true = function-like
	var_arg string    // either "" or name of vararg parameter
	param   []string  // parameter names of a function-like macro
	expand  []*Token  // can be empty
}

func NewCppState(lex *Lexer) *CppState {
	cpp := new(CppState)
	cpp.lex = lex
	cpp.stack = make([]BlkMode, 0)
	cpp.lines = make([]*LexLine, 0)
	return cpp
}

// Preprocess reads all the tokens (LexLines) from the lexer
// and does some really cool preprocessing stuff.
func (cpp *CppState) Preprocess() cmError {
	for {
		ll := cpp.lex.ScanLine()
		if ll == nil {
			break
		}
		if ll.Empty() {
			continue
		}

		ErrorPos_FromToken(ll.Tokens[0])

		if ll.HasError() {
			etok := ll.Tokens[0]
			PostError("%s", etok.Str)
			return FAILED
		}

		if cpp.ProcessLine(ll) != OKAY {
			return FAILED
		}
	}

	// check for unclosed #if..endif blocks
	if !cpp.StackEmpty() {
		PostError("unclosed #if..#endif detected")
		return FAILED
	}

	// replace sequences of strings with a single one
	cpp.ConcatStrings()

	return OKAY
}

//----------------------------------------------------------------------

func (cpp *CppState) ProcessLine(ll *LexLine) cmError {
	if ll.Tokens[0].Match("#") {
		return cpp.Directive(ll)
	} else {
		return cpp.NormalLine(ll)
	}
}

func (cpp *CppState) NormalLine(ll *LexLine) cmError {
	if cpp.StackCombine() {
		// FIXME : handle macros more intelligently

		// FIXME : need to handle func-like macros spanning multiple lines

		cpp.ExpandMacros(ll)

		// add the line to the root CppState
		// [ i.e. maintain the correct order ]
		root := cpp

		if cpp.parent == nil {
			ll.RootFile = true
		}
		for root.parent != nil {
			root = root.parent
		}

		root.lines = append(root.lines, ll)
	}

	return OKAY
}

func (cpp *CppState) Directive(ll *LexLine) cmError {
	ll.PopFront()

	if ll.Empty() {
		// this is a "null" directive, it is allowed and ignored
		return OKAY
	}

	head := ll.PopFront()

	// skip whitespace
	for head.Kind == TOK_Space {
		if ll.Empty() {
			// allow the "null" directive
			return OKAY
		}
		head = ll.PopFront()
	}

	if head.Kind != TOK_Name {
		PostError("weird cpp directive after #")
		return FAILED
	}

	// skip whitespace after directive name
	for !ll.Empty() && ll.Tokens[0].IsSpacey() {
		ll.PopFront()
	}

	active := cpp.StackCombine()

	// handle the if/else/endif directives.
	// these MUSt be processed even when in an inactive block.

	switch {
	case head.Match("ifdef"):
		if active {
			b, err2 := cpp.EvalSymbol(ll)
			if err2 != OKAY {
				return FAILED
			}
			cpp.PushStackBool(b)
		} else {
			cpp.PushStack(BM_WASACTIVE)
		}
		return OKAY

	case head.Match("ifndef"):
		if active {
			b, err2 := cpp.EvalSymbol(ll)
			if err2 != OKAY {
				return FAILED
			}
			cpp.PushStackBool(!b)
		} else {
			cpp.PushStack(BM_WASACTIVE)
		}
		return OKAY

	case head.Match("if"):
		if active {
			b, err2 := cpp.EvalExpression(ll)
			if err2 != OKAY {
				return FAILED
			}
			cpp.PushStackBool(b)
		} else {
			cpp.PushStack(BM_WASACTIVE)
		}
		return OKAY

	case head.Match("elif"):
		if cpp.StackEmpty() {
			PostError("found #elif without previous #if")
			return FAILED
		}
		if cpp.StackCombineMost() {
			bm := cpp.StackTop()
			switch bm {
			case BM_INACTIVE:
				b, err2 := cpp.EvalExpression(ll)
				if err2 != OKAY {
					return FAILED
				}
				cpp.PopStack()
				cpp.PushStackBool(b)
			default:
				cpp.ChangeStack(BM_WASACTIVE)
			}
		}
		return OKAY

	case head.Match("else"):
		if cpp.StackEmpty() {
			PostError("found #else without previous #if")
			return FAILED
		}
		if cpp.StackCombineMost() {
			bm := cpp.StackTop()
			switch bm {
			case BM_INACTIVE:
				cpp.ChangeStack(BM_ACTIVE)
			default:
				cpp.ChangeStack(BM_WASACTIVE)
			}
		}
		return OKAY

	case head.Match("endif"):
		if cpp.StackEmpty() {
			PostError("found #endif without previous #if")
			return FAILED
		}

		cpp.PopStack()
		return OKAY
	}

	// other directives are ignored in an inactive block
	if !active {
		return OKAY
	}

	switch {
	case head.Match("include"):
		return cpp.DoInclude(ll)

	case head.Match("include_next"):
		// ignore non-standard rubbish
		return OKAY

	case head.Match("define"):
		return cpp.DoDefine(ll)

	case head.Match("undef"):
		return cpp.DoUndef(ll)

	case head.Match("error"):
		return cpp.DoError(ll)

	case head.Match("warning"):
		// ignored
		return OKAY

	case head.Match("line"):
		// ignored
		return OKAY

	case head.Match("pragma"):
		// ignored
		return OKAY

	default:
		PostError("unknown cpp directive: #%s", head.Str)
		return FAILED
	}

	return OKAY
}

func (cpp *CppState) DoInclude(ll *LexLine) cmError {
	if ll.Empty() {
		PostError("missing filename after #include")
		return FAILED
	}

	// a plain identifier triggers macro expansion
	if ll.Tokens[0].Kind == TOK_Name {
		cpp.ExpandMacros(ll)
	}

	fn_tok := ll.PopFront()

	if fn_tok.Kind == TOK_String || fn_tok.Kind == TOK_Header {
		// ok
	} else {
		PostError("weird filename after #include")
		return FAILED
	}

	fullname := FindHeaderFile(fn_tok.Str, fn_tok.Kind == TOK_Header)
	if fullname == "" {
		// this follows the C standard: try <xxx.h> if "xxx.h" fails
		if fn_tok.Kind == TOK_String {
			fullname = FindHeaderFile(fn_tok.Str, true)
		}

		if fullname == "" {
			var name2 string
			if fn_tok.Kind == TOK_Header {
				name2 = "<" + fn_tok.Str + ">"
			} else {
				name2 = "\"" + fn_tok.Str + "\""
			}
			PostError("cannot find include file: %s", name2)
			return FAILED
		}
	}

	if Options.print_headers {
		Print("  HEADER: %s\n", fullname)
	}

	file, err := os.Open(fullname)
	if err != nil {
		PostError("cannot open file: %s", fullname)
		return FAILED
	}

	lex := NewLexer(fullname, file)
	child_cpp := NewCppState(lex)
	child_cpp.parent = cpp

	err2 := child_cpp.Preprocess()

	file.Close()

	return err2
}

func (cpp *CppState) DoDefine(ll *LexLine) cmError {
	if ll.Empty() {
		PostError("missing name after #define")
		return FAILED
	}

	sym := ll.PopFront()

	if sym.Kind != TOK_Name {
		PostError("missing name after #define")
		return OKAY
	}

	def := cpp.CreateDefine(sym.Str)

	if !ll.Empty() && ll.Tokens[0].Match("(") {
		// function-like macro
		def.funky = true

		ll.PopFront()

		// get the parameters
		for {
			if ll.Empty() {
				PostError("unclosed parameters in #define")
				return OKAY
			}
			par := ll.PopFront()

			if par.Match(")") {
				break
			}
			if par.IsSpacey() {
				continue
			}

			// after a "..." nothing else is allowed, so skip it
			// [ technically an error though ]
			if def.var_arg != "" {
				continue
			}

			var_arg := ""

			if par.Match("...") {
				var_arg = "__VA_ARGS__"
			} else {
				if par.Kind != TOK_Name {
					PostError("bad parameter name in #define")
					return OKAY
				}
				// GNU-style var-args?
				ll.RemoveInitialWhitespace()
				if !ll.Empty() && ll.Tokens[0].Match("...") {
					ll.PopFront()
					var_arg = par.Str
				}
			}

			if var_arg != "" {
				def.var_arg = var_arg
			} else {
				def.param = append(def.param, par.Str)

				// we technically should require a comma or ')' here
				ll.RemoveInitialWhitespace()
				if !ll.Empty() && ll.Tokens[0].Match(",") {
					ll.PopFront()
				}
			}
		}
	}

	ll.RemoveInitialWhitespace()
	ll.RemoveTrailingWhitespace()

	def.expand = ll.Tokens

	/* add the define */

	// technically if the name already exists it should be an error
	// (unless it is identical), but GCC allows it with a warning.
	cpp.AddDefine(def)

	return OKAY
}

func (cpp *CppState) DoUndef(ll *LexLine) cmError {
	if ll.Empty() {
		PostError("missing name after #undef")
		return FAILED
	}

	sym := ll.PopFront()

	if sym.Kind != TOK_Name {
		// technically an error, but safe to be lax here
		return OKAY
	}

	cpp.RemoveDefine(sym.Str)
	return OKAY
}

func (cpp *CppState) DoError(ll *LexLine) cmError {
	PostError("#error %s", ll.Stringify())
	return FAILED
}

//----------------------------------------------------------------------

func (cpp *CppState) EvalSymbol(ll *LexLine) (bool, cmError) {
	if ll.Empty() {
		PostError("missing symbol after #ifdef or #ifndef")
		return false, FAILED
	}

	sym := ll.PopFront()

	return cpp.doEvalSymbol(sym), OKAY
}

func (cpp *CppState) doEvalSymbol(sym *Token) bool {
	if sym.Kind != TOK_Name {
		// technically an error, but safe to be lax here
		return false
	}

	switch sym.Str {
	case "__FILE__", "__LINE__", "__DATE__", "__TIME__":
		return true
	}

	def := all_defines[sym.Str]

	return (def != nil)
}

func (cpp *CppState) EvalExpression(ll *LexLine) (bool, cmError) {
	ll.RemoveWhitespace()

	if ll.Empty() {
		PostError("missing expression after #if or #elif")
		return false, FAILED
	}

	cpp.ReplaceDefinedChecks(ll)
	cpp.ExpandMacros(ll)

	// macros can supply more whitespace
	ll.RemoveWhitespace()

	// as per C standard, remaining identifiers become "0"
	cpp.ReplaceIdentifiers(ll)

	val, err2 := cpp.ParseConstExpr(ll.Tokens[:])

	return (val != 0), err2
}

func (cpp *CppState) ParseConstExpr(tokens []*Token) (int64, cmError) {
	// use Dijkstra's shunting yard to parse the expression

	i := 0
	size := len(tokens)
	seen := false

	var term int64

	op_stack    := make([]string, 0)
	unary_stack := make([]string, 0)
	term_stack  := make([]int64, 0)

	if size == 0 {
		PostError("empty #if expression")
		return -1, FAILED
	}

	shunt := func() {
		// pop the operator
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0:len(op_stack)-1]

		if len(term_stack) >= 2 {
			// pop the left and right terms
			L := term_stack[len(term_stack)-2]
			R := term_stack[len(term_stack)-1]
			term_stack = term_stack[0 : len(term_stack)-2]

			term = cpp.ApplyBinaryOp(op, L, R)
			term_stack = append(term_stack, term)

//Print("APPLY %d %s %d --> %d\n", L, op, R, term)
		}
	}

	for i < size {
		t := tokens[i]
		i++

		if t.Match(")") {
			PostError("stray ')' in #if expression")
			return -1, FAILED
		}

		if t.Match("(") {
			// find closing parenthesis
			start := i
			deep  := 0

			for {
				if i >= size {
					PostError("missing ')' in #if expression")
					return -1, FAILED
				}
				t2 := tokens[i]
				i++

				if t2.Match("(") {
					deep += 1
				} else if t2.Match(")") {
					if deep == 0 { break }
					deep -= 1
				}
			}

			sub_expr := tokens[start:i-1]

			var err2 cmError

			term, err2 = cpp.ParseConstExpr(sub_expr)
			if err2 != OKAY {
				return -1, FAILED
			}

			goto have_term
		}

		// hack to prevent dying on the ?: ternary sword
		if t.Match("?") {
			start := i
			deep  := 0

			for {
				if i >= size {
					PostError("missing ':' in #if expression")
					return -1, FAILED
				}
				t2 := tokens[i]
				i++

				if t2.Match("?") {
					deep += 1
				} else if t2.Match(":") {
					if deep == 0 { break }
					deep -= 1
				}
			}

			mid_expr := tokens[start:i-1]
			_ = mid_expr

			// TODO recursively evaluate the mid expression.
			// however I'm not sure what to do with it.  current code
			// for '?' simply computes the expression after ':'.
		}

		if t.Kind == TOK_Symbol {
			// should be an operator

			op := t.Str

			if !seen {
				// unary operator

				switch op {
				case "!", "~", "+", "-":
					// ok
				default:
					PostError("unknown unary operator in #if expression: '%s'", op)
					return -1, FAILED
				}

				unary_stack = append(unary_stack, op)

			} else {
				// binary operator
				prec := OperatorPrecedence(op)
				if prec < 0 {
					PostError("unknown binary operator in #if expression: '%s'", op)
					return -1, FAILED
				}

				// shunt existing operators if they have greater precedence
				for len(op_stack) > 0 {
					top := op_stack[len(op_stack)-1]
					top_prec := OperatorPrecedence(top)

					if top_prec <= prec {
						shunt()
						continue
					}
					break
				}

				op_stack = append(op_stack, op)
				seen = false
			}

			continue
		}

		// should be a terminal

		switch t.Kind {
		case TOK_Int:
			var err2 cmError
			term, err2 = DecodeInt(t.Str)
			if err2 != OKAY {
				return -1, FAILED
			}

		case TOK_Char:
			// convert char to integer
			term = 0
			r := []rune(t.Str)
			if len(r) > 0 {
				term = int64(r[0])
			}

		case TOK_Float, TOK_String:
			PostError("cannot use float/string in #if expression")
			return -1, FAILED

		default:
			PostError("weird token in #if expression")
			return -1, FAILED
		}

	have_term:
		if seen {
			// actually an error to have two values in a row.
			// but we are being lax and ignoring it....
			continue
		}

		seen = true

		// apply the unary operators
		for len(unary_stack) > 0 {
			// pop the operator
			op := unary_stack[len(unary_stack)-1]
			unary_stack = unary_stack[0:len(unary_stack)-1]

//Print("APPLY %s %d --> %d\n", op, term, cpp.ApplyUnaryOp(op, term))
			term = cpp.ApplyUnaryOp(op, term)
		}

		term_stack = append(term_stack, term)
	}

	if !seen || len(unary_stack) > 0 {
		PostError("missing value in #if expression")
		return -1, FAILED
	}

	// handle remaining binary operators
	for len(op_stack) > 0 {
		shunt()
	}

	if len(term_stack) > 0 {
		term = term_stack[0]
	}

	return term, OKAY
}

func OperatorPrecedence(op string) int {
	// NOTE: lower number is stronger associativity

	switch op {
	case "*", "/", "%": return 5
	case "+", "-":   return 6
	case "<<", ">>": return 7
	case "<", "<=", ">", ">=": return 9
	case "==", "!=": return 10
	case "&":  return 11
	case "^":  return 12
	case "|" : return 13
	case "&&": return 14
	case "||": return 15
	case "?":  return 16
	case ",":  return 17

	default: return -1 // unknown operator
	}
}

func OperatorPrecedence2(op string) int {
	if IsAssignmentOperator(op) {
		return 16
	} else {
		return OperatorPrecedence(op)
	}
}

func IsAssignmentOperator(op string) bool {
	switch op {
	case "=", "+=", "-=", "*=", "/=", "%=",
		 "<<=", ">>=", "&=", "|=", "^=":
		return true

	default:
		return false
	}
}

func (cpp *CppState) ApplyUnaryOp(op string, V int64) int64 {
	switch op {
	case "+": return V
	case "-": return -V
	case "~": return ^V
	case "!": return BoolToInt(V == 0)

	default:
		panic("bad unary op: " + op)
	}
}

func (cpp *CppState) ApplyBinaryOp(op string, L, R int64) int64 {
	switch op {
	case "+" : return L + R
	case "-" : return L - R
	case "*" : return L * R
	case "/" : if R == 0 { return 0 } ; return L / R
	case "%" : if R == 0 { return 0 } ; return L % R
	case "<<": return L << uint64(R)
	case ">>": return L >> uint64(R)
	case "&" : return L & R
	case "^" : return L ^ R
	case "|" : return L | R
	case "?" : return R
	case "," : return R

	case "<" : return BoolToInt(L <  R)
	case "<=": return BoolToInt(L <= R)
	case ">" : return BoolToInt(L >  R)
	case ">=": return BoolToInt(L >= R)
	case "==": return BoolToInt(L == R)
	case "!=": return BoolToInt(L != R)
	case "&&": return BoolToInt((L != 0) && (R != 0))
	case "||": return BoolToInt((L != 0) || (R != 0))

	default:
		panic("bad binary op: " + op)
	}
}

func (cpp *CppState) ReplaceDefinedChecks(ll *LexLine) {
	// find instances of "defined X" or "defined ( X )" and replace
	// them with either a "0" or "1" token.  invalid usage of the
	// "defined" keyword is ignored.

	size := len(ll.Tokens)
	changes := false

	for i, tok := range ll.Tokens {
		if tok.Kind == TOK_DEAD || !tok.Match("defined") {
			continue
		}

		tok.Kind = TOK_DEAD
		changes  = true

		if i+1 < size {
			sym := ll.Tokens[i+1]

			if sym.Match("(") && i+2 < size {
				sym.Kind = TOK_DEAD
				if i+3 < size && ll.Tokens[i+3].Match(")") {
					ll.Tokens[i+3].Kind = TOK_DEAD
				}
				sym = ll.Tokens[i+2]
			}

			result := "0"
			if cpp.doEvalSymbol(sym) {
				result = "1"
			}

			sym.Kind = TOK_Int
			sym.Str  = result
		}
	}

	if changes {
		ll.RemoveDuds()
	}
}

func (cpp *CppState) ReplaceIdentifiers(ll *LexLine) {
	for _, tok := range ll.Tokens {
		if tok.Kind == TOK_Name {
			tok.Kind = TOK_Int
			tok.Str  = "0"
		}
	}
}

func (cpp *CppState) ExpandMacros(ll *LexLine) {
	active := make(map[string]bool)
	cpp.ExpandMacros2(ll, active)
}

func (cpp *CppState) ExpandMacros2(ll *LexLine, active map[string]bool) {
	// Note that we don't produce any errors here, even for constructs
	// which are technically illegal.

	new_list := make([]*Token, 0)

	for !ll.Empty() {
		sym := ll.PopFront()

		// find a macro name
		if sym.Kind == TOK_Name {
			if active[sym.Str] {
				// must not expand already active macros.
				// e.g. some macros are like: #define FOO FOO
			} else {
				def := all_defines[sym.Str]

				if def != nil {
					// for function-like macros, this removes the actual pars from ll
					exp := cpp.PerformExpansion(ll, sym, def, active)

					// insert the expansion into the line being built
					for _, t := range exp.Tokens {
						new_list = append(new_list, t)
					}
					continue
				}
			}
		}

		// handle some pre-defined macro names
		if sym.Kind == TOK_Name {
			switch sym.Str {
			case "__FILE__":
				sym.Kind = TOK_String
				sym.Str  = sym.FileName

			case "__LINE__":
				sym.Kind = TOK_Int
				sym.Str  = fmt.Sprintf("%d", sym.LineNum)

			case "__DATE__":
				sym.Kind = TOK_String
				sym.Str  = "Nov 11 1111"  // dummy date

			case "__TIME__":
				sym.Kind = TOK_String
				sym.Str  = "11:11:11"  // dummy time
			}
		}

		new_list = append(new_list, sym)
	}

	ll.Tokens = new_list
}

func (cpp *CppState) NewActive(old map[string]bool, name string) map[string]bool {
	res := make(map[string]bool)
	for o_name, _ := range old {
		res[o_name] = true
	}
	res[name] = true
	return res
}

func (cpp *CppState) PerformExpansion(ll *LexLine, sym *Token, def *Define, active map[string]bool) *LexLine {
	expansion := newLexLine()

	if !def.funky {
		// object-like macros are quite easy
		for _, tok := range def.expand {
			t2 := tok.Clone(true)
			t2.FileName = sym.FileName
			t2.LineNum  = sym.LineNum
			expansion.Add(t2)
		}

	} else {
		// require an opening '(' parenthesis
		if ll.Empty() || ! ll.Tokens[0].Match("(") {
			return ll
		}
		ll.PopFront()

		pars := make(map[string]*LexLine)

		cpp.ReadActualParameters(def, ll, pars)

		// expand the contents of each parameter
		for _, par := range pars {
			par.RemoveInitialWhitespace()
			par.RemoveTrailingWhitespace()

			cpp.ExpandMacros2(par, active)
		}

		k    := 0
		klen := len(def.expand)

		// perform the expansion
		for k < klen {
			tok := def.expand[k]
			k++

			// check for "#" followed by a parameter
			if tok.Match("#") {
				if k < klen {
					pname := def.expand[k]
					if pname.Kind == TOK_Name && pars[pname.Str] != nil {
						pval := pars[pname.Str]

						// create a string token
						t2 := NewNode(TOK_String)
						t2.Str = pval.Stringify()
						t2.FileName = sym.FileName
						t2.LineNum  = sym.LineNum

						expansion.Add(t2)

						k++
						continue
					}
				}
			}

			// check for a parameter name
			if tok.Kind == TOK_Name && pars[tok.Str] != nil {
				pval := pars[tok.Str]

				for _, ptok := range pval.Tokens {
					t2 := ptok.Clone(true)
					t2.FileName = sym.FileName
					t2.LineNum  = sym.LineNum
					expansion.Add(t2)
				}
				continue
			}

			// ordinary thing
			t2 := tok.Clone(true)
			t2.FileName = sym.FileName
			t2.LineNum  = sym.LineNum
			expansion.Add(t2)
		}
	}

	cpp.HandleHashHash(expansion)

	// rescan the new group of tokens
	// [ not strictly correct, but good enough I hope ]
	cpp.ExpandMacros2(expansion, cpp.NewActive(active, def.name))

	return expansion
}

func (cpp *CppState) ReadActualParameters(def *Define, ll *LexLine, pars map[string]*LexLine) {
	// create storage for each parameter
	for _, name := range def.param {
		pars[name] = newLexLine()
	}

	d_idx := 0  // destination param

	deep := 0  // for sub-exprs in '()' parentheses

	for !ll.Empty() {
		t := ll.PopFront()

		if t.Match("(") {
			deep += 1
			continue
		}

		if t.Match(")") {
			if deep == 0 {
				break
			}
			deep -= 1
			continue
		}

		// once we reach the var-arg, ALL tokens go into it
		if def.var_arg != "" && d_idx == len(def.param)-1 {
			goto add_tok
		}

		if t.Match(",") && deep == 0 {
			d_idx += 1
			continue
		}

	add_tok:
		// ignore excess parameters
		if d_idx < len(def.param) {
			d_ll := pars[def.param[d_idx]]
			d_ll.Add(t.Clone(true))
		}
	}
}

func (cpp *CppState) HandleHashHash(exp *LexLine) {
	size := len(exp.Tokens)

	for i, t := range exp.Tokens {
		if t.Kind == TOK_Symbol && t.Match("##") {
			L := i - 1
			R := i + 1

			if L >= 0 && exp.Tokens[L].IsSpacey() { L-- }
			if R < size && exp.Tokens[R].IsSpacey() { R++ }

			if L >= 0 && R < size {
				if exp.Tokens[L].IsJoinable() && exp.Tokens[R].IsJoinable() {
					for k := L; k <= R; k++ {
						exp.Tokens[k].Kind = TOK_DEAD
					}

					t.Kind = TOK_Name
					t.Str  = exp.Tokens[L].Str + exp.Tokens[R].Str
				}
			}
		}
	}

	exp.RemoveDuds()
}

//----------------------------------------------------------------------

func (cpp *CppState) CreateDefine(name string) *Define {
	def := new(Define)
	def.name = name
	def.param = make([]string, 0)
	def.expand = make([]*Token, 0)
	return def
}

func (cpp *CppState) AddDefine(def *Define) {
	all_defines[def.name] = def
}

func (cpp *CppState) RemoveDefine(name string) {
	delete(all_defines, name)
}

func (def *Define) Add(t *Token) {
	def.expand = append(def.expand, t)
}

//----------------------------------------------------------------------

func (cpp *CppState) PushStack(bm BlkMode) {
	cpp.stack = append(cpp.stack, bm)
}

func (cpp *CppState) PushStackBool(b bool) {
	if b {
		cpp.PushStack(BM_ACTIVE)
	} else {
		cpp.PushStack(BM_INACTIVE)
	}
}

func (cpp *CppState) PopStack() {
	slen := len(cpp.stack)
	if slen > 0 {
		cpp.stack = cpp.stack[0:slen-1]
	}
}

func (cpp *CppState) ChangeStack(bm BlkMode) {
	slen := len(cpp.stack)
	if slen > 0 {
		cpp.stack[slen-1] = bm
	}
}

func (cpp *CppState) StackEmpty() bool {
	return len(cpp.stack) == 0
}

func (cpp *CppState) StackTop() BlkMode {
	slen := len(cpp.stack)
	if slen > 0 {
		return cpp.stack[slen-1]
	}
	return BM_ACTIVE
}

func (cpp *CppState) StackCombine() bool {
	for _, bm := range cpp.stack {
		if bm != BM_ACTIVE {
			return false
		}
	}
	return true
}

func (cpp *CppState) StackCombineMost() bool {
	for i := 0; i < len(cpp.stack)-1; i++ {
		bm := cpp.stack[i]
		if bm != BM_ACTIVE {
			return false
		}
	}
	return true
}

//----------------------------------------------------------------------

func (cpp *CppState) NextLine() *LexLine {
	// Note: this is destructive

	if len(cpp.lines) > 0 {
		ll := cpp.lines[0]
		cpp.lines = cpp.lines[1:]
		return ll
	}

	// no more tokens
	return nil
}

func (cpp *CppState) DumpCooked() {
	for {
		ll := cpp.NextLine()
		if ll == nil { break }
		for _, t := range ll.Tokens {
			fn := t.FileName
			if len(fn) > 24 {
				fn = fn[len(fn)-24:]
			}
			c := ' '
			if ll.RootFile { c = 'R' }
			Print("%c %-24s :%5d : %s\n", c, fn, t.LineNum, t.String())
		}
	}
}

func (cpp *CppState) ConcatStrings() {
	var last *Token

	for _, ll := range cpp.lines {
		for _, t := range ll.Tokens {
			if t.Kind == TOK_String {
				if last == nil {
					last = t
				} else {
					// perform concatenation
					last.Str = last.Str + t.Str

					t.Kind = TOK_Space
					t.Str  = " "
				}
				continue
			}

			if !t.IsSpacey() {
				last = nil
			}
		}
	}
}
