// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// A "node" is a token which has been analysed and given an ND_XXX
// kind and/or extra information in the Info field.  The code in
// this file is primarily concerned with syntax, it is *the* place
// where special syntax is decoded into a simpler node tree.

// import "fmt"

const (
	ND_INVALID TokenKind = 100 + iota

	ND_VarDecl   // Str = name, Info.ty is type, child is initializer (if any)
	ND_FuncDecl  // Str = name, Info.ty is type, child is body (if any)
	ND_TypeDef   // Str = name, Info.ty is type

	ND_Var       // Str = name.
	ND_Field     // Str = name, child is struct/union value
	ND_Label     // Str = name, child is real statement.
	ND_TypeSize  // Info.ty = type
	ND_TypeAlign // Info.ty = type

	ND_Operator  // left   | right.  Info.op is operator.
	ND_Index     // array  | index
	ND_Call      // func   | params....

	ND_Block     // zero or more child elements (statements or var decls)
	ND_Empty     // the empty statement
	ND_Continue  //
	ND_Break     //
	ND_Goto      // Str = name
	ND_Return    // child is expression or absent

	ND_If        // cond | true-statement | false-statement (opt)
	ND_While     // cond | body
	ND_Do        // cond | body
	ND_For       // init | cond | post | body

	ND_Switch    // first child is ND_Block, other are ND_Case or ND_Default
	ND_Case      // first child is value, children are statements
	ND_Default   // children are statements (zero or more)
)

// NodeInfo provides extra information for each Token (i.e. node)
// in the code tree.
type NodeInfo struct {
	ty *Type
	op string
}

// P_FAIL represents a parsing error from a ParseXXX function.
var P_FAIL *Token = nil
var T_FAIL *Type  = nil

type PState struct {
	cpp *CppState

	// current token
	tok *Token

	// current line of tokens
	ll *LexLine
}

func NewParseState(cpp *CppState) *PState {
	ps := new(PState)
	ps.cpp = cpp
	return ps
}

func NewNode(kind TokenKind) *Token {
	nd := new(Token)
	nd.Kind = kind
	nd.Children = make([]*Token, 0)
	return nd
}

func (t *Token) String2() string {
	// this handles the ND_XXX node types defined in the file

	switch t.Kind {
	case ND_VarDecl:   return "VarDecl '"  + t.Str + "'"
	case ND_FuncDecl:  return "FuncDecl '" + t.Str + "'"
	case ND_TypeDef:   return "TypeDef '"  + t.Str + "'"

	case ND_Var:       return "Var '"  + t.Str + "'"
	case ND_Field:     return "Field '"  + t.Str + "'"
	case ND_Label:     return "Label '"  + t.Str + "'"
	case ND_TypeSize:  return "TypeSize"
	case ND_TypeAlign: return "TypeAlign"

	case ND_Operator:  return "Operator '"  + t.Info.op + "'"
	case ND_Index:     return "Index"
	case ND_Call:      return "Call"

	case ND_Block:     return "Block"
	case ND_Empty:     return "Empty"
	case ND_Continue:  return "Continue"
	case ND_Break:     return "Break"
	case ND_Goto:      return "Goto '" + t.Str + "'"
	case ND_Return:    return "Return"

	case ND_If:        return "If"
	case ND_While:     return "While"
	case ND_Do:        return "Do"
	case ND_For:       return "For"
	case ND_Switch:    return "Switch"
	case ND_Case:      return "Case"
	case ND_Default:   return "Default"

	default:
		return "!!!INVALID!!!"
	}
}

func (nd *Token) InfoString() string {
	s := ""

	// show type information, but not too long
	if nd.Info.ty != nil {
		ts := nd.Info.ty.String()
		if len(ts) > 32 {
			ts = ts[0:30] + "..."
		}

		s = "type:" + ts
	}

	return s
}

func (t *Token) Dump() {
	t.doDump(0)
}

func (t *Token) doDump(level int) {
	Print("line %4d: %*s%s %s\n", t.LineNum, level, "",
		t.String(), t.InfoString())

	if t.Children != nil {
		for _, child := range t.Children {
			child.doDump(level + 2)
		}
	}
}

//----------------------------------------------------------------------

// Parser takes the preprocessed (normal) tokens and then
// does some mind-blowingly cool shit with them.
func (ps *PState) Parser() cmError {
	ps.Next()

	for {
		// end of file?
		if ps.tok == nil {
			return OKAY
		}

		decl := ps.ParseDeclaration()
		if decl == P_FAIL {
			return FAILED
		}

		if decl.Kind == ND_FuncDecl && len(decl.Children) > 0 {
			// no ; is needed
		} else if !ps.Match(";") {
			PostError("expected ; after global declaration, %s", ps.GotToken())
			return FAILED
		}
		ps.Next()

		if ps.ConsumeDecl(decl) != OKAY {
			return FAILED
		}
	}
}

func (ps *PState) ConsumeDecl(decl *Token) cmError {
	switch decl.Kind {
	case ND_TypeDef:
		return CreateUserType(decl)

	// FIXME other stuff!

	default:
		// TEMP CRUD
		decl.Dump()
		return OKAY
	}
}

// Match checks that the current token matches the given string,
// but handles the EOF condition too.
func (ps *PState) Match(s string) bool {
	if ps.tok == nil {
		return false
	}
	return ps.tok.Match(s)
}

// Expect checks that the current token matches the given string.
// if there is no match, an error is posted.
func (ps *PState) Expect(s string) bool {
	if ps.Match(s) {
		return true
	}
	PostError("expected %s, %s", s, ps.GotToken())
	return false
}

// GotToken is used for error messages.
func (ps *PState) GotToken() string {
	if ps.tok == nil {
		return "reached EOF"
	} else {
		return "got: " + ps.tok.String()
	}
}

// Next reads the next token into "tok" field.
// it returns that token, or NIL if reached EOF.
func (ps *PState) Next() *Token {
	for {
		if ps.cpp == nil {
			ps.tok = nil
			return nil
		}

		if ps.ll == nil || ps.ll.Empty() {
			ps.ll = ps.cpp.NextLine()
			if ps.ll == nil {
				ps.cpp = nil
			} else {
				ps.ll.RemoveWhitespace()
			}
			continue
		}

		ps.tok = ps.ll.PopFront()
		ErrorPos_FromToken(ps.tok)
		return ps.tok
	}
}

// Peek reads the token after the current one, i.e. the one which
// a call to Next() would normally give.  returns NIL on EOF.
func (ps *PState) Peek() *Token {
	for {
		if ps.cpp == nil {
			return nil
		}

		if ps.ll == nil || ps.ll.Empty() {
			ps.ll = ps.cpp.NextLine()
			if ps.ll == nil {
				ps.cpp = nil
			} else {
				ps.ll.RemoveWhitespace()
			}
			continue
		}

		return ps.ll.Tokens[0]
	}
}

//----------------------------------------------------------------------

func (ps *PState) ParseDeclaration() *Token {
	qual := QUAL_NONE

	for {
		flag := ps.IsQualifier(ps.tok)
		if flag == QUAL_NONE {
			break
		}
		qual |= flag

		if ps.Next() == nil {
			PostError("expected type, got EOF")
			return P_FAIL
		}
	}

	decl := NewNode(ND_VarDecl)

	if (qual & QUAL_TypeDef) != 0 {
		decl.Kind = ND_TypeDef
	}

	ty := ps.ParseType()
	if ty == T_FAIL {
		return P_FAIL
	}

	if ps.tok == nil {
		PostError("expected identifier")
		return P_FAIL
	}

	// a forward struct/union/enum declaration?
	if ps.Match(";") &&
		(ty.base == TYP_Struct || ty.base == TYP_Union || ty.base == TYP_Enum) &&
		ty.tag != "" {

		return NewNode(ND_Empty)
	}

	if ps.tok.Kind != TOK_Name {
		PostError("expected identifier, got: %s", ps.tok.String())
		return P_FAIL
	}
	decl.Str = ps.tok.Str

	ps.Next()

	for {
		if ps.Match("[") {
			ty = ps.ParseType_Array(ty)
			if ty == T_FAIL {
				return P_FAIL
			}
			continue
		}
		if ps.Match("(") {
			ty = ps.ParseType_Function(ty)
			if ty == T_FAIL {
				return P_FAIL
			}
			decl.Kind = ND_FuncDecl
			break
		}

		break
	}
	decl.Info.ty = ty

	if ps.Match("{") {
		body := ps.ParseCodeBlock()
		if body == P_FAIL {
			return P_FAIL
		}
		decl.Add(body)

	} else if ps.Match("=") {
		ps.Next()

		exp := ps.ParseExpression()
		if exp == P_FAIL {
			return P_FAIL
		}
		decl.Add(exp)
	}

	// semicolon is checked by parent

	return decl
}

func (ps *PState) HaveDeclaration() bool {
	if ps.tok == nil {
		return false
	}

	if ps.IsQualifier(ps.tok) != QUAL_NONE {
		return true
	}
	if ps.IsKnownType(ps.tok) != TFLAG_NONE {
		return true
	}
	if ps.tok.Kind == TOK_Name {
		if user_types[ps.tok.Str] != nil {
			return true
		}
	}
	if ps.Match("struct") || ps.Match("union") || ps.Match("enum") {
		return true
	}
	return false
}

type Qualifier int
const (
	QUAL_NONE Qualifier = 0

	QUAL_Extern Qualifier = (1 << iota)
	QUAL_Static
	QUAL_Inline
	QUAL_Register
	QUAL_Auto
	QUAL_TypeDef
)

func (ps *PState) IsQualifier(t *Token) Qualifier {
	switch {
	case t.Match("extern"):    return QUAL_Extern
	case t.Match("static"):    return QUAL_Static
	case t.Match("inline"):    return QUAL_Inline
	case t.Match("register"):  return QUAL_Register
	case t.Match("auto"):      return QUAL_Auto
	case t.Match("typedef"):   return QUAL_TypeDef
	}
	// not a qualifier
	return QUAL_NONE
}

func (ps *PState) HaveBracedType() bool {
	t := ps.tok
	if t == nil { return false }
	if !t.Match("(") { return false }

	t2 := ps.Peek()
	if t2 == nil { return false }

	if ps.IsKnownType(t2) != TFLAG_NONE {
		return true
	}
	if t2.Kind == TOK_Name {
		if user_types[t2.Str] != nil {
			return true
		}
	}
	if t2.Match("struct") || t2.Match("union") || t2.Match("enum") {
		return true
	}
	return false
}

func (ps *PState) ParseBracedType() *Type {
	if !ps.Match("(") {
		PostError("expected type in (), %s", ps.GotToken())
		return T_FAIL
	}
	ps.Next()

	ty := ps.ParseType()
	if ty == T_FAIL { return T_FAIL }

	if !ps.Match(")") {
		PostError("expected ) after type in (), %s", ps.GotToken())
		return T_FAIL
	}
	ps.Next()
	return ty
}

func (ps *PState) ParseType() *Type {
	// here we are only checking for a pointer

	ty := ps.ParseType2()
	if ty == T_FAIL {
		return T_FAIL
	}

	for ps.tok != nil {
		if ps.Match("const") {
			ps.Next()
			continue
		}

		if ps.Match("*") {
			ps.Next()
			ty = ty.MakePointer()
			continue
		}

		break
	}

	return ty
}

func (ps *PState) ParseType2() *Type {
	// a user type?
	if ps.tok.Kind == TOK_Name {
		user := user_types[ps.tok.Str]
		if user != nil {
			ps.Next()
			return user
		}
	}

	switch {
	case ps.Match("struct"):
		return ps.ParseType_Struct(false)

	case ps.Match("union"):
		return ps.ParseType_Struct(true)

	case ps.Match("enum"):
		return ps.ParseType_Enum()

	case ps.Match("__builtin_va_list"):
		ps.Next()
		return valist_type
	}

	tflags := TFLAG_NONE

	for ps.tok != nil {
		fl := ps.IsKnownType(ps.tok)
		if fl == TFLAG_NONE {
			break
		}
		if fl == TFLAG_Long && (tflags & TFLAG_Long) != 0 {
			tflags |= TFLAG_LongLong
		} else {
			tflags |= fl
		}

		ps.Next()
	}

	is_const := ((tflags & TFLAG_Const) != 0)
	tflags = tflags & ^TFLAG_Const

	// const is mostly ignored
	_ = is_const

	if tflags == 0 {
		PostError("expected type, %s", ps.GotToken())
		return T_FAIL
	}

	is_signed   := ((tflags & TFLAG_Signed) != 0)
	is_unsigned := ((tflags & TFLAG_Unsigned) != 0)
	tflags = tflags & ^(TFLAG_Signed | TFLAG_Unsigned)

	// by themselves, "signed" and "unsigned" imply an int
	if tflags == 0 {
		tflags |= TFLAG_Int
	}

	switch {
	case (tflags & TFLAG_Complex) != 0:
		// this is unsupported, but checked later on
		return complex_type

	case (tflags & TFLAG_Int128) != 0:
		// this is unsupported, but checked later on
		return s128_type

	case (tflags & TFLAG_Float128) != 0:
		// this is unsupported, but checked later on
		return f128_type

	case (tflags & TFLAG_Double) != 0:
		return f64_type

	case (tflags & TFLAG_Float) != 0:
		return f32_type

	case (tflags & TFLAG_Void) != 0:
		return void_type

	case (tflags & TFLAG_Bool) != 0:
		return bool_type

	case (tflags & TFLAG_LongLong) != 0:
		if is_unsigned {
			return u64_type
		} else {
			return s64_type
		}

	case (tflags & TFLAG_Long) != 0:
		if is_unsigned {
			return u32_type
		} else {
			return s32_type
		}

	case (tflags & TFLAG_Int) != 0:
		if is_unsigned {
			return u32_type
		} else {
			return s32_type
		}

	case (tflags & TFLAG_Short) != 0:
		if is_unsigned {
			return u16_type
		} else {
			return s16_type
		}

	case (tflags & TFLAG_Char) != 0:
		// our chars are unsigned by default
		if is_signed {
			return s8_type
		} else {
			return u8_type
		}

	default:
		PostError("weird type specifier")
		return T_FAIL
	}
}

// TODO volatile and restrict keywords (here??)

type Typeflag int
const (
	TFLAG_NONE Typeflag = 0

	TFLAG_Void Typeflag = (1 << iota)
	TFLAG_Bool

	TFLAG_Char
	TFLAG_Short
	TFLAG_Int
	TFLAG_Long
	TFLAG_LongLong
	TFLAG_Int128

	TFLAG_Float
	TFLAG_Double
	TFLAG_Float128
	TFLAG_Complex

	TFLAG_Signed
	TFLAG_Unsigned

	TFLAG_Const
)

func (ps *PState) IsKnownType(t *Token) Typeflag {
	switch {
	case t.Match("void"):   return TFLAG_Void
	case t.Match("_Bool"):  return TFLAG_Bool

	case t.Match("char"):   return TFLAG_Char
	case t.Match("short"):  return TFLAG_Short
	case t.Match("int"):    return TFLAG_Int
	case t.Match("long"):   return TFLAG_Long

	case t.Match("float"):       return TFLAG_Float
	case t.Match("_Float32"):    return TFLAG_Float
	case t.Match("double"):      return TFLAG_Double
	case t.Match("_Float64"):    return TFLAG_Double

	case t.Match("_Float128"):   return TFLAG_Float128
	case t.Match("__float128"):  return TFLAG_Float128
	case t.Match("_Complex"):    return TFLAG_Complex
	case t.Match("__complex__"): return TFLAG_Complex

	case t.Match("signed"):    return TFLAG_Signed
	case t.Match("unsigned"):  return TFLAG_Unsigned

	case t.Match("const"):     return TFLAG_Const
	}

	// not a built-in C type
	return TFLAG_NONE
}

func (ps *PState) ParseType_Struct(union bool) *Type {
	ps.Next() // skip "struct"

	// we either create a new type or find a tagged one
	var ty *Type
	var tag string

	if ps.tok != nil && ps.tok.Kind == TOK_Name {
		tag = ps.tok.Str
		ty = tagged_types[tag]

		ps.Next()
	}

	if ty == nil {
		if union {
			ty = NewType(TYP_Union, 0)
		} else {
			ty = NewType(TYP_Struct, 0)
		}
		ty.incomplete = true
		ty.tag = tag

		if tag != "" {
			tagged_types[tag] = ty
		}
	}

	if !ps.Match("{") {
		if tag == "" {
			PostError("expected { after struct/union, %s", ps.GotToken())
			return T_FAIL
		}

		return ty
	}
	ps.Next()

	// we define the type here, so clear incomplete flag
	ty.incomplete = false

	for {
		if ps.Match("}") {
			ps.Next()
			break
		}

		var decl *Token

		if !ps.Match(":") {
			decl = ps.ParseDeclaration()
			if decl == P_FAIL {
				return T_FAIL
			}
		}

		if ps.Match(":") {
			ps.Next()
			if ps.tok == nil { break }

			// SHIT, need to parse a const expression
			PostError("bit-fields NYI")
			return T_FAIL
		}

		// add field to type
		// FIXME add field to type
		_ = decl

		if !ps.Match(";") {
			PostError("expected ; after field, %s", ps.GotToken())
			return T_FAIL
		}
		ps.Next();
	}

	return ty
}

func (ps *PState) ParseType_Enum() *Type {
	ps.Next() // skip "enum"

	// we either create a new type or find a tagged one
	var ty *Type
	var tag string

	if ps.tok != nil && ps.tok.Kind == TOK_Name {
		tag = ps.tok.Str
		ty = tagged_types[tag]

		ps.Next()
	}

	if ty == nil {
		ty = NewType(TYP_Enum, 0)
		ty.incomplete = true
		ty.tag = tag

		if tag != "" {
			tagged_types[tag] = ty
		}
	}

	if !ps.Match("{") {
		if tag == "" {
			PostError("expected { after enum, %s", ps.GotToken())
			return T_FAIL
		}
		return ty
	}
	ps.Next()

	// we define the type here, so clear incomplete flag
	ty.incomplete = false

	for {
		if ps.Match("}") {
			ps.Next()
			break
		}

		if ps.tok.Kind != TOK_Name {
			PostError("expected enumerator name, got: %s", ps.tok.String())
			return T_FAIL
		}
		name := ps.tok.Str

		ps.Next()
		if ps.tok == nil { break }

		if ps.Match("=") {
			ps.Next()
			if ps.tok == nil { break }

			// SHIT, need to parse a const expression
			PostError("enumerator values NYI")
			return T_FAIL
		}

		// add enumerator to type
		// FIXME add enumerator to type
		_ = name

		if ps.Match(",") {
			ps.Next()
		}
	}

	return ty
}

func (ps *PState) ParseType_Array(elem_type *Type) *Type {
	ps.Next()  // skip '['

	ty := NewType(TYP_Array, 0)
	ty.sub = elem_type

	if ps.Match("]") {
		ty.open = true
	} else {
		size_expr := ps.ParseExpression()
		if size_expr == P_FAIL { return T_FAIL }

		size, err2 := EvalIntExpression(size_expr)
		if err2 != OKAY { return T_FAIL }
		if size <= 0 || int64(int(size)) != size {
			PostError("bad array size: %d", size)
			return T_FAIL
		}
		ty.size = int(size)

		if !ps.Match("]") {
			PostError("expected ] after array size, %s", ps.GotToken())
			return T_FAIL
		}
	}

	ps.Next()  // skip ']'

	return ty
}

func (ps *PState) ParseType_Function(ret_type *Type) *Type {
	ps.Next()  // skip '('

	ty := NewType(TYP_Function, 0)
	ty.sub = ret_type

//Print("FUNCTION TYPE....\n")
	for {
		if ps.tok == nil {
			PostError("unclosed function parameters")
			return T_FAIL
		}
		if ps.Match(")") {
			ps.Next()
			break
		}

		if ps.Match("...") {
			ps.Next()
			ty.var_args = true

			if !ps.Match(")") {
				PostError("expected ) after '...' parameter")
				return T_FAIL
			}
			ps.Next()
			break
		}

		// hack to handle: int Blah(void)
		if ps.Match("void") {
			t := ps.Peek()
			if t != nil && t.Match(")") {
				ps.Next()
				continue
			}
		}

		param := ps.ParseDeclaration()
		if param == P_FAIL {
			return T_FAIL
		}

		ty.AddParam(param.Str, param.Info.ty)
//Print("   PARAMETER %s : %s\n", param.Str, param.Info.ty.String())

		if ps.Match(",") {
			ps.Next()
		}
	}

	return ty
}

//----------------------------------------------------------------------

func (ps *PState) ParseBracketExpr(keyword string) *Token {
	if !ps.Match("(") {
		PostError("expected ( after %s, got %s", keyword, ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	exp := ps.ParseExpression()
	if exp == P_FAIL { return P_FAIL }

	if !ps.Match(")") {
		PostError("expected ) after %s expr, got %s", keyword, ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	return exp
}

func (ps *PState) ParseExpression() *Token {
	// use Dijkstra's shunting yard to parse the expression

	seen := false

	var term *Token

	op_stack    := make([]string, 0)
	unary_stack := make([]string, 0)
	term_stack  := make([]*Token, 0)

	shunt := func() {
		// pop the operator
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0:len(op_stack)-1]

		if len(term_stack) >= 2 {
			// pop the left and right terms
			L := term_stack[len(term_stack)-2]
			R := term_stack[len(term_stack)-1]
			term_stack = term_stack[0 : len(term_stack)-2]

			term = NewNode(ND_Operator)
			term.Info.op = op
			term.Add(L)
			term.Add(R)

			term_stack = append(term_stack, term)

//Print("APPLY %d %s %d --> %d\n", L, op, R, term)
		}
	}

	for ps.tok != nil {
		t := ps.tok

		if t.Match(")") || t.Match("]") || t.Match("}") ||
			t.Match(",") || t.Match(";") || t.Match(":") {
			break
		}

		// special handling for sizeof, which can take a type in ()
		if t.Match("sizeof") || t.Match("_Alignof") {
			op := t.Str
			ps.Next()

			if ps.HaveBracedType() {
				ty := ps.ParseBracedType()
				if ty == T_FAIL { return P_FAIL }
				term = NewNode(ND_TypeSize)
				if op == "_Alignof" { term.Kind = ND_TypeAlign }
				goto have_term

			} else {
				unary_stack = append(unary_stack, op)
				ps.Next()
				continue
			}
		}

		if t.Kind == TOK_Symbol && !t.Match("(") {
			// should be an operator

			op := t.Str

			if !seen {
				// unary operator

				switch op {
				case "!", "~", "+", "-",
					 "++", "--", "*", "&":
					// ok

				default:
					PostError("unknown unary operator: '%s'", op)
					return P_FAIL
				}

				unary_stack = append(unary_stack, op)

			} else {
				// binary operator
				prec := -1
				right_assoc := false

				prec = OperatorPrecedence2(op)
				if prec < 0 {
					PostError("unknown binary operator: '%s'", op)
					return P_FAIL
				}
				if IsAssignmentOperator(op) || op == "?" {
					right_assoc = true
				}

				if op == "?" {
					// FIXME ParseTernaryBits
				}

				// shunt existing operators if they have greater precedence
				for len(op_stack) > 0 {
					top := op_stack[len(op_stack)-1]
					top_prec := OperatorPrecedence2(top)

					if top_prec < prec || (top_prec == prec && !right_assoc) {
						shunt()
						continue
					}

					break
				}

				op_stack = append(op_stack, op)
				seen = false
			}

			ps.Next()
			continue
		}

		// should be a terminal

		term = ps.ParseTerminal()
		if term == P_FAIL {
			return P_FAIL
		}

	have_term:
		if seen {
			PostError("two values in a row in expression")
			return P_FAIL
		}

		seen = true

		// apply the unary operators
		for len(unary_stack) > 0 {
			// pop the operator
			op := unary_stack[len(unary_stack)-1]
			unary_stack = unary_stack[0:len(unary_stack)-1]

			old_term := term

			term = NewNode(ND_Operator)
			term.Info.op = op
			term.Add(old_term)

//Print("APPLY %s %d --> %d\n", op, term, cpp.ApplyUnaryOp(op, term))
		}

		term_stack = append(term_stack, term)
	}

	if !seen || len(unary_stack) > 0 {
		PostError("missing value in expression")
		return P_FAIL
	}

	// handle remaining binary operators
	for len(op_stack) > 0 {
		shunt()
	}

	if len(term_stack) > 0 {
		term = term_stack[0]
	}

	return term
}

func (ps *PState) ParseTerminal() *Token {
	// here we handle array access / function calls

	term := ps.ParseTerminal2()
	if term == P_FAIL { return P_FAIL }

	for ps.tok != nil {
		switch {
		case ps.Match("["):
			ps.Next()
			idx := ps.ParseExpression()
			if idx == P_FAIL { return P_FAIL }

			if !ps.Match("]") {
				PostError("expected ] after index, %s", ps.GotToken())
				return P_FAIL
			}
			t2 := NewNode(ND_Index)
			t2.Add(term)
			t2.Add(idx)
			term = t2

			ps.Next()  // skip ']'
			continue

		case ps.Match("("):
			ps.Next()

			t2 := NewNode(ND_Call)
			t2.Add(term)

			// parse the arguments to a func call
			for {
				if ps.tok == nil {
					PostError("expected ) after function call, %s",
						ps.GotToken())
					return P_FAIL
				}

				if ps.Match(")") {
					ps.Next()
					break
				}

				par := ps.ParseExpression()
				if par == P_FAIL { return P_FAIL }
				t2.Add(par)

				if ps.Match(",") {
					ps.Next()
				}
			}

			term = t2
			continue

		case ps.Match("."):
			term = ps.ParseFieldAccess(term, false)
			if term == P_FAIL { return P_FAIL }
			continue

		case ps.Match("->"):
			term = ps.ParseFieldAccess(term, true)
			if term == P_FAIL { return P_FAIL }
			continue

		case ps.Match("++"):
			t2 := NewNode(ND_Operator)
			t2.Info.op = "++2"
			t2.Add(term)
			term = t2

			ps.Next()
			continue

		case ps.Match("--"):
			t2 := NewNode(ND_Operator)
			t2.Info.op = "--2"
			t2.Add(term)
			term = t2

			ps.Next()
			continue
		}

		break
	}

	return term
}

func (ps *PState) ParseTerminal2() *Token {
	t := ps.tok

	// currently literals are just used in their original form.
	// [ rather than a ND_Literal node ]

	switch t.Kind {
	case TOK_Int, TOK_Char, TOK_Float, TOK_String:
		ps.Next()
		return t
	}

	// assume a name is a variable
	if t.Kind == TOK_Name {
		ps.Next()

		t.Kind = ND_Var
		return t
	}

	if t.Match("(") {
		ps.Next()

		child := ps.ParseExpression()
		if child == P_FAIL {
			return P_FAIL
		}

		if !ps.Match(")") {
			PostError("expected ) after sub-expr, %s", ps.GotToken())
			return P_FAIL
		}

		ps.Next()
		return child
	}

	PostError("expected value for expression, got: %s", t.String())
	return P_FAIL
}

func (ps *PState) ParseFieldAccess(term *Token, via_ptr bool) *Token {
	ps.Next()

	if ps.tok == nil || ps.tok.Kind != TOK_Name {
		PostError("expected field name after '.' or '->'")
		return P_FAIL
	}

	t2 := NewNode(ND_Field)
	t2.Str = ps.tok.Str

	ps.Next()

	if via_ptr {
		t3 := NewNode(ND_Operator)
		t3.Info.op = "*"
		t3.Add(term)

		t2.Add(t3)
	} else {
		t2.Add(term)
	}

	return t2
}

//----------------------------------------------------------------------

func (ps *PState) ParseCodeBlock() *Token {
	block := NewNode(ND_Block)
	block.LineNum = ps.tok.LineNum

	ps.Next()  // skip '{'

	for {
		if ps.tok == nil {
			PostError("unclosed code block, reached EOF")
			return P_FAIL
		}
		if ps.Match("}") {
			ps.Next()
			break
		}

		stat := ps.ParseStatement()
		if stat == P_FAIL { return P_FAIL }

		block.Add(stat)
	}

	return block
}

func (ps *PState) ParseStatement() *Token {
	// check for variable declarations
	if ps.HaveDeclaration() {
		decl := ps.ParseDeclaration()
		if decl == P_FAIL { return P_FAIL }
		return ps.CheckSemicolon(decl)
	}

	// check for label name followed by ":"
	var label *Token
	if ps.tok.Kind == TOK_Name {
		peek := ps.Peek()
		if peek != nil && peek.Match(":") {
			label = NewNode(ND_Label)
			label.Str = ps.tok.Str

			ps.Next()  // skip name
			ps.Next()  // skip ':'
		}
	}

	stat := ps.ParseStatement2()
	if stat == P_FAIL { return P_FAIL }

	if label != nil {
		label.Add(stat)
		return label
	}

	return stat
}

func (ps *PState) ParseStatement2() *Token {
	nd := ps.tok

	switch {
	case ps.Match("if"):
		return ps.Statement_If()

	case ps.Match("switch"):
		return ps.Statement_Switch()

	case ps.Match("while"):
		return ps.Statement_While()

	case ps.Match("for"):
		return ps.Statement_For()

	case ps.Match("do"):
		return ps.Statement_Do()

	case ps.Match("goto"):
		return ps.Statement_Goto()

	case ps.Match("return"):
		return ps.Statement_Return()

	case ps.Match("continue"):
		nd.Kind = ND_Continue
		ps.Next()
		return ps.CheckSemicolon(nd)

	case ps.Match("break"):
		nd.Kind = ND_Break
		ps.Next()
		return ps.CheckSemicolon(nd)

	case ps.Match("{"):
		return ps.ParseCodeBlock()

	case ps.Match(";"):
		// the null statement
		nd.Kind = ND_Empty
		ps.Next()
		return nd
	}

	exp := ps.ParseExpression()
	if exp == P_FAIL { return P_FAIL }

	if !ps.Match(";") {
		PostError("expected ; after expression, %s", ps.GotToken())
		return P_FAIL
	}

	ps.Next()
	return exp
}

func (ps *PState) Statement_If() *Token {
	ps.Next()

	cond := ps.ParseBracketExpr("if")
	if cond == P_FAIL { return P_FAIL }

	b_then := ps.ParseStatement()
	if b_then == P_FAIL { return P_FAIL }

	nd := NewNode(ND_If)

	nd.Add(cond)
	nd.Add(b_then)

	if ps.Match("else") {
		ps.Next()

		b_else := ps.ParseStatement()
		if b_else == P_FAIL { return P_FAIL }

		nd.Add(b_else)
	}

	return nd
}

func (ps *PState) Statement_Switch() *Token {
	ps.Next()

	cond := ps.ParseBracketExpr("switch")
	if cond == P_FAIL { return P_FAIL }

	// NOTE: technically any type of statement can follow the condition,
	//       but we don't support that.
	if !ps.Match("{") {
		PostError("expected { for switch statement, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	nd := NewNode(ND_Switch)

	dest := NewNode(ND_Block)
	nd.Add(dest)

	for {
		if ps.tok == nil {
			PostError("unclosed code block, reached EOF")
			return P_FAIL
		}
		if ps.Match("}") {
			ps.Next()
			break
		}

		if ps.Match("case") {
			ps.Next()

			val := ps.ParseExpression()
			if val == P_FAIL { return P_FAIL }

			if !ps.Match(":") {
				PostError("expected : after case value, %s", ps.GotToken())
				return P_FAIL
			}
			ps.Next()

			// new destination for statements
			dest = NewNode(ND_Case)
			dest.Add(val)

			nd.Add(dest)
			continue
		}

		if ps.Match("default") {
			ps.Next()
			if !ps.Match(":") {
				PostError("expected : after default, %s", ps.GotToken())
				return P_FAIL
			}
			ps.Next()

			dest = NewNode(ND_Default)
			nd.Add(dest)
			continue
		}

		stat := ps.ParseStatement()
		if stat == P_FAIL { return P_FAIL }

		dest.Add(stat)
	}

	return nd
}

func (ps *PState) Statement_While() *Token {
	ps.Next()

	cond := ps.ParseBracketExpr("while")
	if cond == P_FAIL { return P_FAIL }

	body := ps.ParseStatement()
	if body == P_FAIL { return P_FAIL }

	nd := NewNode(ND_While)

	nd.Add(cond)
	nd.Add(body)

	return nd
}

func (ps *PState) Statement_For() *Token {
	ps.Next()

	if !ps.Match("(") {
		PostError("expected ( after for, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	var init *Token
	var cond *Token
	var post *Token

	if ps.Match(";") {
		init = NewNode(ND_Empty)
	} else if ps.HaveDeclaration() {
		init = ps.ParseDeclaration()
	} else {
		init = ps.ParseExpression()
	}
	if init == P_FAIL { return P_FAIL }

	if !ps.Match(";") {
		PostError("expected ; between for exprs, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	if ps.Match(";") {
		cond = NewNode(ND_Empty)
	} else {
		cond = ps.ParseExpression()
		if cond == P_FAIL { return P_FAIL }
	}

	if !ps.Match(";") {
		PostError("expected ; between for exprs, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	if ps.Match(")") {
		post = NewNode(ND_Empty)
	} else {
		post = ps.ParseExpression()
		if post == P_FAIL { return P_FAIL }
	}

	if !ps.Match(")") {
		PostError("expected ) after for exprs, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	body := ps.ParseStatement()
	if body == P_FAIL { return P_FAIL }

	nd := NewNode(ND_For)
	nd.Add(init)
	nd.Add(cond)
	nd.Add(post)
	nd.Add(body)

	return nd
}

func (ps *PState) Statement_Do() *Token {
	ps.Next()

	stat := ps.ParseStatement()
	if stat == P_FAIL { return P_FAIL }

	if !ps.Match("while") {
		PostError("expected while after do statement, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()

	cond := ps.ParseBracketExpr("while")
	if cond == P_FAIL { return P_FAIL }

	nd := NewNode(ND_Do)
	nd.Add(cond)
	nd.Add(stat)

	return ps.CheckSemicolon(nd)
}

func (ps *PState) Statement_Goto() *Token {
	ps.Next()

	if ps.tok == nil || ps.tok.Kind != TOK_Name {
		PostError("expected name after goto, %s", ps.GotToken())
		return P_FAIL
	}

	nd := NewNode(ND_Goto)
	nd.Str = ps.tok.Str

	ps.Next()

	return ps.CheckSemicolon(nd)
}

func (ps *PState) Statement_Return() *Token {
	ps.Next()

	nd := NewNode(ND_Return)

	if ps.Match(";") {
		ps.Next()
		return nd
	}

	exp := ps.ParseExpression()
	if exp == P_FAIL { return P_FAIL }

	nd.Add(exp)

	return ps.CheckSemicolon(nd)
}

func (ps *PState) CheckSemicolon(nd *Token) *Token {
	if !ps.Match(";") {
		PostError("expected ; after statement, %s", ps.GotToken())
		return P_FAIL
	}
	ps.Next()
	return nd
}
