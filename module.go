// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"

type Module struct {
	name  string
	files []*ModFile
}

var modules []*Module

type ModFile struct {
	dir string
	fn  string
}

func NewModule(name string) *Module {
	mod := new(Module)
	mod.name = name
	mod.files = make([]*ModFile, 0)

	modules = append(modules, mod)
	return mod
}

func LoadModules(path string) error {
	modules = make([]*Module, 0)

	f, err := os.Open(path)
	if err != nil {
		FatalError("error opening modules.def")
	}

	lex := NewLexer(path, f)

	err = ReadModuleDefs(lex)

	f.Close()

	return err
}

func ReadModuleDefs(lex *Lexer) error {
	for {
		start := lex.BasicScan()
		if start == nil {
			break
		}
		if start.IsError() {
			return fmt.Errorf("line %d: %s", start.LineNum, start.Str)
		}
		if !start.Match("module") {
			return fmt.Errorf("line %d: expected 'module' keyword", start.LineNum)
		}

		id := lex.BasicScan()
		if id == nil || id.Kind != TOK_Name {
			return fmt.Errorf("missing name for module def")
		}

		open := lex.BasicScan()
		if open == nil || !open.Match("{") {
			return fmt.Errorf("missing { for module def")
		}

		mod := NewModule(id.Str)

		for {
			keyword := lex.BasicScan()
			if keyword == nil {
				return fmt.Errorf("unclosed module def")
			}
			if keyword.IsError() {
				return fmt.Errorf("line %d: %s", keyword.LineNum, keyword.Str)
			}

			if keyword.Match("}") {
				break
			}

			err := mod.ReadKeyword(lex, keyword)
			if err != nil {
				return err
			}
		}
	}

	if len(modules) == 0 {
		return fmt.Errorf("no modules defined!")
	}

	return Ok
}

func (mod *Module) ReadKeyword(lex *Lexer, keyword *Token) error {
	if keyword.Kind != TOK_Name {
		return fmt.Errorf("bad syntax in module def")
	}

	word := keyword.Str

	switch word {
	case "dir", "file":
		// ok
	default:
		return fmt.Errorf("unknown keyword '%s' in module def", word)
	}

	var arg1 *Token
	var arg2 *Token

	cur_dir := "."

	if true {
		arg1 = lex.BasicScan()
		if arg1 == nil || arg1.Kind != TOK_String {
			return fmt.Errorf("bad/missing argument to '%s' in module", word)
		}

		switch word {
		case "dir":
			cur_dir = arg1.Str

		case "file":
			mf := new(ModFile)
			mf.dir = cur_dir
			mf.fn  = arg1.Str
		}
	}

	_ = arg2

	term := lex.BasicScan()
	if term == nil || !term.Match(";") {
		return fmt.Errorf("missing ; in module def")
	}

	return Ok
}

//----------------------------------------------------------------------


