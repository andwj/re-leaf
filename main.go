// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	in_files []string

	include_dirs []string
	sys_inc_dirs []string

	print_headers bool
	print_types   bool
	help bool
}

var out_fp *os.File

//----------------------------------------------------------------------

func main() {
	// this can fatal error
	HandleArgs()

	ClearErrors()
	SetupTypes()

	// visit all files, even if some produce an error
	for _, in_file := range Options.in_files {
		VisitFile(in_file)
	}

	if HaveErrors() {
		ShowAllErrors()
		FatalError("some errors occurred")
	}

	os.Exit(0)
}

func HandleArgs() {
	Options.in_files = make([]string, 0)
	Options.print_types = true

	argv.Multi("I", "iquote",  &Options.include_dirs, "dir", "specify normal include dirs")
	argv.Multi("J", "isystem", &Options.sys_inc_dirs, "dir", "specify system include dirs")
	argv.Gap()
	argv.Enabler("H", "headers", &Options.print_headers, "display visited header files")
	argv.Enabler("T", "types",   &Options.print_types,   "display created types")
	argv.Gap()
	argv.Enabler("h", "help", &Options.help, "display help text and exit")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	names := argv.Unparsed()

	if Options.help || len(names) == 0 {
		ShowUsage()
		os.Exit(0)
	}

	if len(names) == 0 {
		FatalError("missing input filenames")
	}
	Options.in_files = names

	if len(Options.include_dirs) == 0 {
		Options.include_dirs = append(Options.include_dirs, ".")
	}
	if len(Options.sys_inc_dirs) == 0 {
		Options.sys_inc_dirs = append(Options.sys_inc_dirs, "/usr/local/include")
		Options.sys_inc_dirs = append(Options.sys_inc_dirs, "/usr/include")
	}
}

func ShowUsage() {
	Print("Usage: yb-leaf FILE.lf [OPTIONS...]\n")

	Print("\n")
	Print("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	if out_fp != nil {
		out_fp.Close()
	}
	format = "yb-leaf: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(1)
}

func Print(format string, a ...interface{}) {
	format = fmt.Sprintf(format, a...)
	fmt.Printf("%s", format)
}

func FileExists(fn string) bool {
	f, err := os.Open(fn)
	if err == nil {
		f.Close()
	}
	return (err == nil)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

//----------------------------------------------------------------------

func VisitFile(fullname string) cmError {
	ErrorPos_File(fullname)

	file, err := os.Open(fullname)
	if err != nil {
		PostError("no such file: %s", fullname)
		return FAILED
	}

	Print("FILE: %s\n", fullname)

	all_defines = make(map[string]*Define)

	lex := NewLexer(fullname, file)
//	lex.DumpLines()

	cpp := NewCppState(lex)
	cpp.Preprocess()
//	cpp.DumpCooked()

	ps := NewParseState(cpp)
	ps.Parser()

	file.Close()

	return OKAY
}

func FindHeaderFile(basename string, std bool) string {
	list := Options.include_dirs
	if std {
		list = Options.sys_inc_dirs
	}

	// very basic check that the basename is valid
	if basename == "" || basename == "." || basename == ".." || basename == "/" {
		return ""
	}

	for _, dirname := range list {
		fullname := filepath.Join(dirname, basename)
		if fullname != "" {
			if FileExists(fullname) {
				return fullname
			}
		}
	}

	// not found
	return ""
}
