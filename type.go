// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"

type BaseType int

const (
	TYP_Void BaseType = iota

	TYP_Int
	TYP_Float
	TYP_Pointer
	TYP_Bool
	TYP_Enum

	TYP_Array
	TYP_Struct
	TYP_Union
	TYP_Function
)

type Type struct {
	base BaseType

	// for TYP_Int and TYP_Float, this is number of bits (8/16/32/64).
	// for TYP_Array, this is number of elements (0 if open).
	// for everything else, this is zero.
	size int

	// the total size of this type (in bytes).
	// -1 means it has not (or could not) be determined yet.
	// not used for open arrays (is zero).
	// for open structs, it is the minimum size.
	// for unions, it is size of the largest field.
	// for pointers, this is always 8 (since arch is 64-bit).
	byte_len int

	// distinguishes u8..u64 from s8..s64
	unsigned bool

	// for struct/union/enum, the tag name
	tag string

	// for types (like "struct foo") without a known structure.
	// [ these can be completed by a future declaration ]
	incomplete bool

	// for TYP_Array and TYP_Struct, indicates it has no fixed size
	open bool

	// this is pointed-to type for pointers, element type for arrays,
	// and return type for functions.
	sub *Type

	// parameters of a function, fields of a struct or union.
	param []Parameter

	// last parameter was the `...` symbol
	var_args bool
}

type Parameter struct {
	// name of function parameter or struct/union field
	name string

	// its type
	ty *Type
}

var (
	void_type *Type
	bool_type *Type

	s8_type   *Type
	s16_type  *Type
	s32_type  *Type
	s64_type  *Type

	u8_type   *Type
	u16_type  *Type
	u32_type  *Type
	u64_type  *Type

	f32_type  *Type
	f64_type  *Type

	// some unsupported types
	s128_type *Type
	f128_type *Type

	complex_type *Type
	valist_type  *Type

	// all typedefs go here
	user_types map[string]*Type

	// all tagged structs/unions/enums go here
	tagged_types map[string]*Type
)

func SetupTypes() {
	void_type = NewType(TYP_Void, 0)
	bool_type = NewType(TYP_Bool, 1)

	s8_type  = NewType(TYP_Int, 8)
	s16_type = NewType(TYP_Int, 16)
	s32_type = NewType(TYP_Int, 32)
	s64_type = NewType(TYP_Int, 64)

	u8_type  = NewType(TYP_Int, 8)  ; u8_type.unsigned  = true
	u16_type = NewType(TYP_Int, 16) ; u16_type.unsigned = true
	u32_type = NewType(TYP_Int, 32) ; u32_type.unsigned = true
	u64_type = NewType(TYP_Int, 64) ; u64_type.unsigned = true

	f32_type = NewType(TYP_Float, 32)
	f64_type = NewType(TYP_Float, 64)

	// three unsupported things
	s128_type = NewType(TYP_Int, 128)
	f128_type = NewType(TYP_Float, 128)
	complex_type = NewType(TYP_Float, 777)
	valist_type = NewType(TYP_Float, 666)

	user_types   = make(map[string]*Type)
	tagged_types = make(map[string]*Type)
}

func NewType(base BaseType, size int) *Type {
	ty := new(Type)
	ty.base = base
	ty.size = size
	ty.param = make([]Parameter, 0)

	if base == TYP_Int || base == TYP_Float {
		ty.byte_len = size / 8
	} else if base == TYP_Pointer {
		// assumes 64-bit architecture
		ty.byte_len = 8
	} else if base == TYP_Void {
		ty.byte_len = 0
	} else {
		// actual size computed later
		ty.byte_len = -1
	}

	return ty
}

func CreateUserType(nd *Token) cmError {
	// allow type to already exist, it is almost certainly a type
	// defined by multiple processing of the same header file.
	if user_types[nd.Str] != nil {
		return OKAY
	}

	user_types[nd.Str] = nd.Info.ty

	if Options.print_types {
		Print("USER TYPE: %s\n", nd.Str)
	}

	return OKAY
}

//----------------------------------------------------------------------

func (ty *Type) MakePointer() *Type {
	new_ty := NewType(TYP_Pointer, 0)
	new_ty.sub = ty

	return new_ty
}

func (ty *Type) AddParam(name string, par_ty *Type) {
	ty.param = append(ty.param, Parameter{name: name, ty: par_ty})
}

func (ty *Type) String() string {
	if ty == nil {
		return "NIL!!"
	}

	switch ty.base {
	case TYP_Void: return "void"
	case TYP_Bool: return "bool"

	case TYP_Int:
		s := "s"
		if ty.unsigned {
			s = "u"
		}
		return fmt.Sprintf("%s%d", s, ty.size)

	case TYP_Float:
		return fmt.Sprintf("f%d", ty.size)

	case TYP_Pointer:
		return "^ " + ty.sub.String()

	case TYP_Enum:
		if ty.tag != "" {
			return "enum " + ty.tag
		} else {
			return "enum"
		}

	case TYP_Struct:
		if ty.tag != "" {
			return "struct " + ty.tag
		} else {
			return "struct"
		}

	case TYP_Union:
		if ty.tag != "" {
			return "union " + ty.tag
		} else {
			return "union"
		}

	case TYP_Array:
		return "array " + ty.sub.String()

	case TYP_Function:
		return "func"

	default:
		return "????"
	}
}
