// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

//----------------------------------------------------------------------

func EvalIntExpression(nd *Token) (int64, cmError) {
	res, err2 := EvalIntExpression2(nd)
	if err2 == OKAY { Print("EvalIntExpression --> %d\n", res) }
	return res, err2
}

func EvalIntExpression2(nd *Token) (int64, cmError) {
	switch nd.Kind {
	case TOK_Int:
		return EvalInt_Integer(nd)

	case TOK_Char:
		return EvalInt_Char(nd)

	case TOK_Float:
		PostError("expected integer expression, got a float")
		return 0, FAILED

	case TOK_String:
		PostError("expected integer expression, got a string")
		return 0, FAILED

	case ND_Operator:
		if nd.Info.op == "?" {
			return EvalInt_TernaryCond(nd)
		} else if len(nd.Children) == 2 {
			return EvalInt_BinaryOp(nd)
		} else {
			return EvalInt_UnaryOp(nd)
		}

	case ND_Var:
		PostError("cannot evaluate var name '%s'", nd.Str)
		return 0, FAILED

	case ND_TypeSize:
		// FIXME
		fallthrough

	case ND_TypeAlign:
		// FIXME
		fallthrough

	default:
		PostError("cannot evaluate: %s", nd.String())
		return 0, FAILED
	}
}

func EvalInt_Integer(nd *Token) (int64, cmError) {
	return DecodeInt(nd.Str)
}

func EvalInt_Char(nd *Token) (int64, cmError) {
	r := []rune(nd.Str)
	if len(r) > 0 {
		return int64(r[0]), OKAY
	}

	PostError("empty character literal")
	return 0, FAILED
}

func EvalInt_UnaryOp(nd *Token) (int64, cmError) {
	V, err2 := EvalIntExpression(nd.Children[0])
	if err2 != OKAY { return 0, FAILED }

	switch nd.Info.op {
	case "+": return  V, OKAY
	case "-": return -V, OKAY
	case "~": return ^V, OKAY
	case "!": return BoolToInt(V == 0), OKAY

	// TODO: sizeof

	default:
		PostError("cannot evaluate unary operator: %s", nd.Info.op)
		return 0, FAILED
	}
}

func EvalInt_BinaryOp(nd *Token) (int64, cmError) {
	// TODO : short-circuit logic for "&&" and "||" ?

	L, err2 := EvalIntExpression(nd.Children[0])
	R, err3 := EvalIntExpression(nd.Children[1])
	if err2 != OKAY || err3 != OKAY { return 0, FAILED }

	switch nd.Info.op {
	case "+" : return L + R, OKAY
	case "-" : return L - R, OKAY
	case "*" : return L * R, OKAY

	case "/" :
		if R == 0 {
			PostError("division by zero in const expression")
			return 0, FAILED
		}
		return L / R, OKAY

	case "%":
		if R == 0 {
			PostError("division by zero in const expression")
			return 0, FAILED
		}
		return L % R, OKAY

	case "<<": return L << uint64(R), OKAY
	case ">>": return L >> uint64(R), OKAY
	case "&" : return L & R, OKAY
	case "^" : return L ^ R, OKAY
	case "|" : return L | R, OKAY
	case "?" : return R, OKAY
	case "," : return R, OKAY

	case "<" : return BoolToInt(L <  R), OKAY
	case "<=": return BoolToInt(L <= R), OKAY
	case ">" : return BoolToInt(L >  R), OKAY
	case ">=": return BoolToInt(L >= R), OKAY
	case "==": return BoolToInt(L == R), OKAY
	case "!=": return BoolToInt(L != R), OKAY

	case "&&": return BoolToInt((L != 0) && (R != 0)), OKAY
	case "||": return BoolToInt((L != 0) || (R != 0)), OKAY

	default:
		PostError("cannot evaluate binary operator: %s", nd.Info.op)
		return 0, FAILED
	}
}

func EvalInt_TernaryCond(nd *Token) (int64, cmError) {
	cond, err2 := EvalIntExpression(nd.Children[0])
	if err2 != OKAY { return 0, FAILED }

	if cond != 0 {
		return EvalIntExpression(nd.Children[1])
	} else {
		return EvalIntExpression(nd.Children[2])
	}
}

func DecodeInt(s string) (int64, cmError) {
	// strip any U/L letters off the end
	for len(s) >= 2 {
		ch := s[len(s)-1]
		if ch == 'l' || ch == 'L' || ch == 'u' || ch == 'U' {
			s = s[0:len(s)-1]
		} else {
			break
		}
	}

	res, err := strconv.ParseInt(s, 0, 64)
	if err != nil {
		PostError("bad integer in #if expression: %s", s)
		return -1, FAILED
	}
	return res, OKAY
}

func BoolToInt(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}
