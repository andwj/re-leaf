// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "fmt"
import "bufio"
import "unicode"
import "unicode/utf8"

type TokenKind int

const (
	TOK_ERROR TokenKind = iota
	TOK_DEAD

	// whitespace.
	// TOK_LongComment has children nodes (TOK_Comment).
	TOK_Space
	TOK_Comment
	TOK_LongComment

	// an identifier or symbol.
	TOK_Name
	TOK_Symbol

	// numeric (etc) literals.
	TOK_Int
	TOK_Float
	TOK_Char
	TOK_String
	TOK_Header
)

type Token struct {
	Kind     TokenKind
	Children []*Token
	Str      string
	Info     NodeInfo

	FileName string
	LineNum  int
}

type Lexer struct {
	reader *bufio.Reader

	filename string
	line_num int
	seen_eof bool

	// state used by ScanLine()
	ml_comment *Token
	ml_line    int

	prev_line *LexLine

	// state used by ScanToken()
	basic_ll *LexLine
}

type LexLine struct {
	Tokens  []*Token
	Indent  int
	LineNum int

	// this is set elsewhere (by CppState.NextLine)
	RootFile bool

	// internal flag -- was this line a // comment?
	sl_comment bool
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(filename string, r io.Reader) *Lexer {
	lexer := new(Lexer)

	lexer.reader = bufio.NewReader(r)
	lexer.filename = filename
	lexer.line_num = 0

	return lexer
}

// DumpLines is a debugging aid, showing each logical line of tokens.
func (lex *Lexer) DumpLines() {
	for {
		ll := lex.ScanLine()

		if ll == nil {
			Print("--- EOF ---\n")
			Print("\n")
			return
		}

		ll.Dump()
	}
}

func (ll *LexLine) Dump() {
	Print("line %d:\n", ll.LineNum)

	if ll.Empty() {
		Print("    blank line\n")
	}

	for _, tok := range ll.Tokens {
		Print("    %s\n", tok.String())
	}
}

// BasicScan is a simple wrapper above ScanLine which returns
// each token in turn, skipping whitespace and comments.
// it is only useful for parsing the module definition file.
// returns NIL for EOF.
func (lex *Lexer) BasicScan() *Token {
	for {
		if lex.basic_ll == nil {
			lex.basic_ll = lex.ScanLine()

			if lex.basic_ll == nil {
				// EOF
				return nil
			}
		}

		if lex.basic_ll.Empty() {
			lex.basic_ll = nil
			continue
		}

		tok := lex.basic_ll.PopFront()
		if !tok.IsSpacey() {
			return tok
		}
	}
}

//----------------------------------------------------------------------

// ScanLine processes one or more lines from the reader, and
// returns either a logical line of tokens or a single TOK_ERROR
// token (which covers both I/O errors and malformed text).
// Once the end of file is reached, nil is returned.
//
// Whitespace at the beginning of a line is translated to an
// "indent level" value.  TABs here are considered to be four
// spaces.  Whitespace at the end of a line is ignored.  In
// other places whitespace becomes a single TOK_Space token.
// Lines which are blank or completely whitespace return a
// LexLine with no tokens.
//
// The '\' backslash occurring at the end of a line (ignoring
// any following whitespace) causes the logical line to be
// continued using the next physical line in the file.  If it
// occurs in a '//' line comment, it continues that comment.
// It is ignored between /* and */ comment markers.
//
// A comment spanning multiple lines, or multiple single line
// comments in a row, become a single TOK_LongComment token.
// Other comments become a TOK_Comment token.  In both cases
// the token is never preceded or followed by TOK_Space, since
// comments are considered to be equivalent to whitespace.
//
// This code does NOT handle preprocessor directives or macro
// expansion -- that is done elsewhere.
//
func (lex *Lexer) ScanLine() *LexLine {
	// this loop is to merge several comment-only lines into a TOK_LongComment
	for {
		prev := lex.prev_line
		cur  := lex.doScanLine()

		if cur == nil /* eof */ {
			lex.prev_line = nil
			return prev
		}

		if prev == nil {
			lex.prev_line = cur
			continue
		}

		if !(prev.sl_comment && cur.sl_comment) {
			lex.prev_line = cur
			return prev
		}

		cmt := prev.Tokens[0]

		if cmt.Kind != TOK_LongComment {
			first := cmt.Clone(false)

			cmt.Kind = TOK_LongComment
			cmt.Add(first)
		}

		cmt.Add(cur.Tokens[0])
	}
}

func (lex *Lexer) doScanLine() *LexLine {
	if lex.seen_eof {
		return nil
	}

	ll := newLexLine()
	ll.Indent = -1
	ll.LineNum = lex.line_num + 1

	for {
		if lex.seen_eof {
			// check for non-terminated multi-line comments
			if lex.ml_comment != nil {
				lex.ml_comment = nil

				// for this error, use line number where comment began
				msg := "unterminated multi-line comment"
				err_tok := lex.newTokenAt(TOK_ERROR, msg, lex.ml_line)

				ll.Add(err_tok)
				return ll
			}

			return nil
		}

		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			ll.Add(err_tok)
			return ll
		}

		if lex.scanRawLine(s, ll) {
			return ll
		}
	}
}

func (lex *Lexer) fetchRawLine() (string, *Token) {
	// need to scan the next line
	lex.line_num += 1

	// NOTE: this can return some data + io.EOF
	s, err := lex.reader.ReadString('\n')

	if err == io.EOF {
		lex.seen_eof = true
	} else if err != nil {
		lex.seen_eof = true
		return "", lex.newToken(TOK_ERROR, err.Error())
	}

	// strip CR and LF from the end
	if len(s) > 0 && s[len(s)-1] == '\n' {
		s = s[0:len(s)-1]
	}
	if len(s) > 0 && s[len(s)-1] == '\r' {
		s = s[0:len(s)-1]
	}

	return s, nil
}

// scanRawLine takes the next line from the file and tokenizes it.
// that line may be a continuation of a previous line.
func (lex *Lexer) scanRawLine(s string, ll *LexLine) bool {
	// convert line to an array of runes
	runes := []rune(s)

	// check for errors
	for _, ch := range runes {
		if ch == utf8.RuneError {
			ll.Add(lex.newToken(TOK_ERROR, "bad utf8 in file"))
			return true
		}
	}

	// calculate indentation level
	indent, r := lex.calcIndentation(runes)

	// only the first real line sets the indent
	if ll.Indent < 0 {
		ll.Indent = indent
	}

	// remove whitespace from end
	for len(r) > 0 && unicode.IsSpace(r[len(r)-1]) {
		r = r[0:len(r)-1]
	}

	is_include := lex.checkInclude(r)

	for {
		// start of multi-line comment?
		if len(r) >= 2 && r[0] == '/' && r[1] == '*' {
			if lex.ml_comment != nil {
				msg := "cannot nest /* */ comments"
				ll.Add(lex.newToken(TOK_ERROR, msg))
				return true
			}

			lex.ml_comment = lex.newToken(TOK_Comment, "")
			r = r[2:]
			continue
		}

		// while inside multi-line comments, only closer is significant
		if lex.ml_comment != nil {
			ml := lex.ml_comment

			if len(r) == 0 {
				// convert to long comment (if not already)
				if ml.Kind != TOK_LongComment {
					lex.ml_comment = lex.newToken(TOK_LongComment, "")
					lex.ml_comment.Add(ml)
				}

				// add a fresh comment token for next line
				cmt := lex.newToken(TOK_Comment, "")
				cmt.LineNum += 1
				lex.ml_comment.Add(cmt)

				// tell caller to feed us a fresh line
				return false
			}

			if len(r) >= 2 && r[0] == '*' && r[1] == '/' {
				if lex.ml_comment == nil {
					msg := "found */ without opening /*"
					ll.Add(lex.newToken(TOK_ERROR, msg))
					return true
				}

				ll.Add(lex.ml_comment)
				lex.ml_comment = nil
				r = r[2:]
				continue
			}

			// add character to current comment
			cmt := lex.ml_comment
			if cmt.Kind == TOK_LongComment {
				cmt = cmt.Children[len(cmt.Children)-1]
			}
			cmt.Str += string(r[0])

			r = r[1:]
			continue
		}

		// end of line?
		if len(r) == 0 {
			return true
		}

		// do we have a joiner?
		if len(r) == 1 && r[0] == '\\' {
			// tell caller to feed us a fresh line
			return false
		}

		// whitespace?
		if unicode.IsSpace(r[0]) {
			if ll.CanAddSpace() {
				spc := lex.newToken(TOK_Space, " ")
				ll.Add(spc)
			}
			r = r[1:]
			continue
		}

		// comment?
		if len(r) >= 2 && r[0] == '/' && r[1] == '/' {
			r := r[2:]
			cmt := lex.newToken(TOK_Comment, string(r))
			if ll.Empty() {
				ll.sl_comment = true
			}
			ll.Add(cmt)
			return true
		}

		// header name?
		if is_include && r[0] == '<' {
			size, tok := lex.decodeHeader(r)

			ll.Add(tok)
			if ll.HasError() { return true }

			r = r[size:]
			continue
		}

		// string?
		if r[0] == '"' || (len(r) >= 2 && r[0] == 'L' && r[1] == '"') {
			size, tok := lex.decodeString(r)

			ll.Add(tok)
			if ll.HasError() { return true }

			// this indicates we exhausted the line (due to '\' at end)
			if size < 0 { return true }

			r = r[size:]
			continue
		}

		// character literal?
		if r[0] == '\'' || (len(r) >= 2 && r[0] == 'L' && r[1] == '\'') {
			size, tok := lex.decodeCharacter(r)

			ll.Add(tok)
			if ll.HasError() { return true }

			r = r[size:]
			continue
		}

		// numeric literal?
		if unicode.IsDigit(r[0]) ||
			(len(r) >= 2 && r[0] == '.' && unicode.IsDigit(r[1])) {

			size, tok := lex.decodeNumber(r)

			ll.Add(tok)
			if ll.HasError() { return true }

			r = r[size:]
			continue
		}

		// anything else MUST be a name / symbol
		size, tok := lex.decodeIdentOrSymbol(r)

		ll.Add(tok)
		if ll.HasError() { return true }

		r = r[size:]
	}

	return false
}

func (lex *Lexer) calcIndentation(r []rune) (int, []rune) {
	indent := 0

	for len(r) > 0 {
		ch := r[0]
		if ch == ' ' {
			indent += 1
		} else if ch == '\t' {
			indent += 4
		} else {
			break
		}

		r = r[1:]
	}

	return indent, r
}

func (lex *Lexer) checkInclude(r []rune) bool {
	for len(r) > 0 && unicode.IsSpace(r[0]) {
		r = r[1:]
	}

	if len(r) == 0 || r[0] != '#' {
		return false
	}

	r = r[1:]

	for len(r) > 0 && unicode.IsSpace(r[0]) {
		r = r[1:]
	}

	if len(r) < 7 {
		return false
	}

	return string(r[0:7]) == "include"
}

//----------------------------------------------------------------------

func (lex *Lexer) decodeHeader(r []rune) (int, *Token) {
	// skip leading '<'
	pos := 1

	s := ""

	for {
		if pos >= len(r) {
			return -1, lex.newToken(TOK_ERROR, "unterminated header name")
		}
		if r[pos] == '>' {
			break
		}

		s = s + string(r[pos])
		pos += 1
	}

	if len(s) == 0 {
		return -1, lex.newToken(TOK_ERROR, "empty header name")
	}

	return pos + 1, lex.newToken(TOK_Header, s)
}

func (lex *Lexer) decodeString(r []rune) (int, *Token) {
	// skip leading quote
	pos := 1

	// long version with 'L' prefix?
	if r[0] == 'L' {
		pos += 1
	}

	s := ""
	exhausted_line := false

	for {
		// hack for strings spanning multiple lines
		if pos == len(r)-1 && r[pos] == '\\' && !lex.seen_eof {
			exhausted_line = true

			s2, err_tok := lex.fetchRawLine()
			if err_tok != nil {
				return -1, err_tok
			}

			s += "\n"

			r = []rune(s2)
			pos = 0
			continue
		}

		if pos >= len(r) {
			return -1, lex.newToken(TOK_ERROR, "unterminated string")
		}
		if r[pos] == '"' {
			break
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) {
			pos += 1

			ch, step := lex.decodeEscape(r[pos:])
			if step == -2 {
				return -1, lex.newToken(TOK_ERROR, "unknown escape in string")
			} else if step < 0 {
				return -1, lex.newToken(TOK_ERROR, "malformed escape in string")
			}

			s = s + string(ch)
			pos += step
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}

	if exhausted_line {
		pos = -10
	}
	return pos + 1, lex.newToken(TOK_String, s)
}

func (lex *Lexer) decodeCharacter(r []rune) (int, *Token) {
	// NOTE: we allow more than one character here, mainly due to
	//       usage of four-character literals in Windows headers.

	// skip leading quote
	pos := 1

	// long version with 'L' prefix?
	if r[0] == 'L' {
		pos += 1
	}

	s := ""

	for {
		if pos >= len(r) {
			return -1, lex.newToken(TOK_ERROR, "unterminated char literal")
		}
		if r[pos] == '\'' {
			break
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) {
			pos += 1

			ch, step := lex.decodeEscape(r[pos:])
			if step == -2 {
				return -1, lex.newToken(TOK_ERROR, "unknown escape in char literal")
			} else if step < 0 {
				return -1, lex.newToken(TOK_ERROR, "malformed escape in char literal")
			}

			s = s + string(ch)
			pos += step
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}

	if len(s) == 0 {
		return -1, lex.newToken(TOK_ERROR, "empty char literal")
	}

	return pos + 1, lex.newToken(TOK_Char, s)
}

func (lex *Lexer) decodeEscape(r []rune) (rune, int) {
	switch r[0] {
	case '"':
		return '"', 1
	case '\'':
		return '\'', 1
	case '\\':
		return '\\', 1
	case 'a':
		return '\a', 1
	case 'b':
		return '\b', 1
	case 'f':
		return '\f', 1
	case 'n':
		return '\n', 1
	case 'r':
		return '\r', 1
	case 't':
		return '\t', 1
	case 'v':
		return '\v', 1
	}

	// this is a GNU-ism
	if r[0] == 'e' {
		return rune(27), 1
	}

	// hexadecimal?
	// (requires two hexadecimal digits, no more, no less)
	if r[0] == 'x' {
		return lex.decodeHexEscape(r, 2)
	}

	// unicode?
	// (these follow conventions of C11 and Go)
	if r[0] == 'u' {
		return lex.decodeHexEscape(r, 4)
	}
	if r[0] == 'U' {
		return lex.decodeHexEscape(r, 8)
	}

	// octal?
	if '0' <= r[0] && r[0] <= '7' {
		pos := 1
		var val rune = r[0] - '0'

		if len(r) >= 2 && ('0' <= r[1] && r[1] <= '7') {
			pos = 2
			val = (val * 8) + r[1] - '0'

			if len(r) >= 3 && ('0' <= r[2] && r[2] <= '7') {
				val = (val * 8) + r[2] - '0'
				pos = 3
			}
		}

		return rune(val), pos
	}

	return utf8.RuneError, -2
}

func (lex *Lexer) decodeHexEscape(r []rune, size int) (rune, int) {
	// this assumes first character is part of the escape (the 'x' or 'u')

	if len(r) < size+1 {
		return utf8.RuneError, -1
	}

	var result rune

	for i := 1; i <= size; i++ {
		result = result << 4
		ch := unicode.ToUpper(r[i])
		if '0' <= ch && ch <= '9' {
			result |= (ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			result |= 10 + (ch - 'A')
		} else {
			return utf8.RuneError, -1
		}
	}

	return result, size + 1
}

// returns a token and # of runes consumed.
// the logic here is for a "preprocessor number", so it is very lax.
func (lex *Lexer) decodeNumber(r []rune) (int, *Token) {
	pos := 1

	seen_dot  := false
	seen_exp  := false

	if r[0] == '.' {
		seen_dot = true
	}

	for pos < len(r) {
		// only allow '-' and '+' after an exponent
		if r[pos] == 'e' || r[pos] == 'E' || r[pos] == 'p' || r[pos] == 'P' {
			pos += 1
			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				pos += 1
			}
			seen_exp = true
			continue
		}

		if r[pos] == '.' {
			pos += 1
			seen_dot = true
			continue
		}

		// support all letters
		if unicode.IsLetter(r[pos]) ||
			unicode.IsDigit(r[pos]) {

			pos += 1
			continue
		}

		break
	}

	kind := TOK_Int
	if seen_dot || seen_exp {
		kind = TOK_Float
	}

	return pos, lex.newToken(kind, string(r[0:pos]))
}

// returns # of runes consumed, or negative on error
func (lex *Lexer) decodeIdentOrSymbol(r []rune) (int, *Token) {
	ch := r[0]
	if unicode.IsLetter(ch) || ch == '_' || ch == '$' {
		return lex.decodeIdentifier(r)
	}

	// check for three-char symbols
	if len(r) >= 3 {
		sym := string(r[0:3])

		switch sym {
		case "...", "<<=", ">>=":
			return 3, lex.newToken(TOK_Symbol, sym)
		}
	}

	// check for two-char symbols
	if len(r) >= 2 {
		sym := string(r[0:2])

		switch sym {
		case
			"::", "==", "!=", ">=", "<=", "<<", ">>",
			"&&", "||", "++", "--", "##", "..", "->",

			"+=", "-=", "*=", "/=", "%=",
			"&=", "|=", "^=":

			return 2, lex.newToken(TOK_Symbol, sym)
		}
	}

	// check for single-char symbols
	if ch >= 33 && ch <= 126 {
		sym := string(r[0:1])
		return 1, lex.newToken(TOK_Symbol, sym)
	}

	// tell caller we could not parse anything
	msg := "illegal character '" + string(r[0]) + "'"
	return -1, lex.newToken(TOK_ERROR, msg)
}

func (lex *Lexer) decodeIdentifier(r []rune) (int, *Token) {
	pos := 1

	for pos < len(r) {
		ch := r[pos]

		if unicode.IsLetter(ch) ||
			unicode.IsDigit(ch) ||
			ch == '_' || ch == '$' {

			// ok
			pos += 1

		} else {
			break
		}
	}

	s := string(r[0:pos])

	return pos, lex.newToken(TOK_Name, s)
}

func (lex *Lexer) newToken(kind TokenKind, str string) *Token {
	t := new(Token)
	t.Kind = kind
	t.Str = str
	t.FileName = lex.filename
	t.LineNum = lex.line_num
	return t
}

func (lex *Lexer) newTokenAt(kind TokenKind, str string, line int) *Token {
	t := new(Token)
	t.Kind = kind
	t.Str = str
	t.FileName = lex.filename
	t.LineNum = line
	return t
}

//----------------------------------------------------------------------

func newLexLine() *LexLine {
	ll := new(LexLine)
	ll.Tokens = make([]*Token, 0)
	return ll
}

func (ll *LexLine) Empty() bool {
	return len(ll.Tokens) == 0
}

func (ll *LexLine) Clear() {
	ll.Indent = 0
	ll.Tokens = ll.Tokens[0:0]
}

func (ll *LexLine) Add(t *Token) {
	// skip everything after an error
	if ll.HasError() {
		return
	}

	// errors must "eat" all previous tokens on a line
	if t.Kind == TOK_ERROR {
		ll.Tokens = ll.Tokens[0:0]
	} else {
		// remove trailing space if adding a comment
		if len(ll.Tokens) > 0 {
			last := ll.Tokens[len(ll.Tokens)-1]
			if last.Kind == TOK_Space && t.IsComment() {
				ll.Tokens = ll.Tokens[0:len(ll.Tokens)-1]
			}
		}
	}

	ll.AddRaw(t)
}

func (ll *LexLine) AddRaw(t *Token) {
	ll.Tokens = append(ll.Tokens, t)
}

func (ll *LexLine) PopFront() *Token {
	res := ll.Tokens[0]
	ll.Tokens = ll.Tokens[1:]
	return res
}

func (ll *LexLine) PopBack() *Token {
	res := ll.Tokens[len(ll.Tokens)-1]
	ll.Tokens = ll.Tokens[0:len(ll.Tokens)-1]
	return res
}

func (ll *LexLine) HasError() bool {
	if ll.Empty() {
		return false
	} else {
		return (ll.Tokens[0].Kind == TOK_ERROR)
	}
}

func (ll *LexLine) CanAddSpace() bool {
	if ll.Empty() {
		return false
	} else {
		last := ll.Tokens[len(ll.Tokens)-1]
		return !last.IsSpacey()
	}
}

func (ll *LexLine) RemoveDuds() {
	new_list := make([]*Token, 0)

	for _, tok := range ll.Tokens {
		if tok.Kind != TOK_DEAD {
			new_list = append(new_list, tok)
		}
	}

	ll.Tokens = new_list
}

func (ll *LexLine) RemoveWhitespace() {
	new_list := make([]*Token, 0)

	for _, tok := range ll.Tokens {
		if !tok.IsSpacey() {
			new_list = append(new_list, tok)
		}
	}

	ll.Tokens = new_list
}

func (ll *LexLine) RemoveInitialWhitespace() {
	for !ll.Empty() && ll.Tokens[0].IsSpacey() {
		ll.PopFront()
	}
}

func (ll *LexLine) RemoveTrailingWhitespace() {
	for !ll.Empty() && ll.Tokens[len(ll.Tokens)-1].IsSpacey() {
		ll.PopBack()
	}
}

func (ll *LexLine) Stringify() string {
	s := ""

	for _, tok := range ll.Tokens {
		switch tok.Kind {
		case TOK_Space, TOK_Comment, TOK_LongComment:
			s = s + " "

		case TOK_Name, TOK_Symbol, TOK_Int, TOK_Float:
			s = s + tok.Str

		case TOK_Char, TOK_String, TOK_Header:
			s = s + fmt.Sprintf("%q", tok.Str)

		default:
			s = s + "??"
		}
	}

	return s
}

//----------------------------------------------------------------------

func (t *Token) Add(child *Token) {
	t.Children = append(t.Children, child)
}

func (t *Token) Prepend(child *Token) {
	t.Children = append(t.Children, nil)
	copy(t.Children[1:], t.Children[0:])
	t.Children[0] = child
}

// Match is a convenience method for checking that the token is a
// TOK_Name matching the given string.
func (t *Token) Match(name string) bool {
	return t.Str == name &&
		(t.Kind == TOK_Name || t.Kind == TOK_Symbol)
}

func (t *Token) IsError() bool {
	return t.Kind == TOK_ERROR
}

func (t *Token) IsSpacey() bool {
	return t.Kind >= TOK_Space && t.Kind <= TOK_LongComment
}

func (t *Token) IsComment() bool {
	return t.Kind == TOK_Comment || t.Kind == TOK_LongComment
}

func (t *Token) IsJoinable() bool {
	return t.Kind == TOK_Name || t.Kind == TOK_Symbol
}

func (t *Token) Replace(other *Token) {
	t.Kind = other.Kind
	t.Info = other.Info
	t.Str = other.Str
	t.Children = other.Children
}

func (t *Token) Clone(deep bool) *Token {
	t2 := new(Token)

	t2.Kind = t.Kind
	t2.Str = t.Str
	t2.FileName = t.FileName
	t2.LineNum = t.LineNum

	if len(t.Children) > 0 {
		t2.Children = make([]*Token, 0)

		for _, child := range t.Children {
			if deep {
				t2.Add(child.Clone(true))
			} else {
				t2.Add(child)
			}
		}
	}

	return t2
}

func (t *Token) String() string {
	if t == nil {
		return "nil"
	}

	switch t.Kind {
	case TOK_ERROR:
		return "ERROR " + ShortSafeString(t.Str)
	case TOK_DEAD:
		return "DEAD"

	case TOK_Space:
		return "Space"
	case TOK_Comment:
		return "Comment"
	case TOK_LongComment:
		return "LongComment  " + fmt.Sprintf("%d lines", len(t.Children))

	case TOK_Name:
		return "Name '" + t.Str + "'"
	case TOK_Symbol:
		return "Symbol '" + t.Str + "'"

	case TOK_Int:
		return "Int '" + t.Str + "'"
	case TOK_Float:
		return "Float '" + t.Str + "'"
	case TOK_Char:
		return "Char " + ShortSafeString(t.Str)
	case TOK_String:
		return "Str " + ShortSafeString(t.Str)
	case TOK_Header:
		return "Header " + ShortSafeString(t.Str)

	// invoke code in parse.go for the ND_XXX kinds
	default:
		return t.String2()
	}
}

func ShortSafeString(s string) string {
	if len(s) > 50 {
		s = s[0:50]
	}

	return fmt.Sprintf("%q", s)
}
